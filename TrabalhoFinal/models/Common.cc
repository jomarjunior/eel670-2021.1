/**
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * Escola Politécnica
 * Departamento de Eletrônica e Computação - DEL
 * 2021.1
 * Linguagens de Programação
 * Professor: Miguel Elias Mitre Campista
 * Alunos:  Bernardo B. Dias
 *          Jomar Júnior de Souza Pereira
 **/

#include "Common.h"

#include <iostream>
#include <string>

std::string
Common::GetCpyRightMessage ()
{
    return "© 2021 Copyright. All rights reserved.";
}

std::string
Common::GetDatabaseConnectionCredentials ()
{
    return  R"(
            host=ec2-3-215-83-124.compute-1.amazonaws.com 
            port=5432 
            dbname=d7rk5r0o8v47uh 
            user=ajmhqvndmolnhd 
            password=2cab1e69f27e8faf09bcb80456fdc0251563178559e5d6962b819c9ba14677a2
            )";
}