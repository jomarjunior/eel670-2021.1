/**
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * Escola Politécnica
 * Departamento de Eletrônica e Computação - DEL
 * 2021.1
 * Linguagens de Programação
 * Professor: Miguel Elias Mitre Campista
 * Alunos:  Bernardo B. Dias
 *          Jomar Júnior de Souza Pereira
 **/

#ifndef COMMON_H
#define COMMON_H

#include <iostream>
#include <string>

class Common
{
    private:

    public:
    static std::string GetCpyRightMessage ();
    static std::string GetDatabaseConnectionCredentials ();
};

#endif