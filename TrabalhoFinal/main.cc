/**
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * Escola Politécnica
 * Departamento de Eletrônica e Computação - DEL
 * 2021.1
 * Linguagens de Programação
 * Professor: Miguel Elias Mitre Campista
 * Alunos:  Bernardo B. Dias
 *          Jomar Júnior de Souza Pereira
 **/

#include <drogon/drogon.h>
#include <drogon/orm/DbClient.h>

#define LISTENER_PORT           8080

drogon::orm::DbClientPtr dbm = NULL;

int main() {
    //Set HTTP listener address and port
    drogon::app().addListener("0.0.0.0", LISTENER_PORT);
    
    drogon::HttpAppFramework::instance().enableSession(1200);

    LOG_DEBUG << "O aplicativo WEB foi inicializado com sucesso!\nAcesse-o em localhost:" << LISTENER_PORT << "/\n";
    LOG_DEBUG << "ctrl + c para encerrar!\n";

    drogon::app().run();
    return 0;
}
