/**
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * Escola Politécnica
 * Departamento de Eletrônica e Computação - DEL
 * 2021.1
 * Linguagens de Programação
 * Professor: Miguel Elias Mitre Campista
 * Alunos:  Bernardo B. Dias
 *          Jomar Júnior de Souza Pereira
 **/

#include "Login.h"
#include "../models/Common.h"
#include "../models/sha256.h"
#include <drogon/orm/DbClient.h>
//add definition of your processing function here

#define USER_ID         "uid"

extern drogon::orm::DbClientPtr dbm;

void
Login::RenderLoginPage(const HttpRequestPtr& request,std::function<void (const HttpResponsePtr &)> &&callback) const
{
    LOG_DEBUG << "Rendering login page\n";
    const drogon::SessionPtr SESSION = request->session();

    HttpViewData viewData;

    trantor::Date now = trantor::Date::date();
    viewData.insert("date", now.toCustomedFormattedStringLocal("%d-%m-%Y as %H:%M:%S", false));
    viewData.insert("cpyright", Common::GetCpyRightMessage());

    // Proibir o usuário de se registrar caso esteja logado
    if (SESSION->find(USER_ID)) {
        // Usuário logado
        LOG_DEBUG << "User already logged in can't register.\n";
        auto response = HttpResponse::newHttpViewResponse("loginLoggedError", viewData);
        callback(response);
    } else {
        // Usuário não logado
        LOG_DEBUG << "User not logged in, proceding to register page.\n";
        auto response = HttpResponse::newHttpViewResponse("loginpage", viewData);
        callback(response);
    }
}

void
Login::ProcessLogin(const HttpRequestPtr& request,std::function<void (const HttpResponsePtr &)> &&callback) const
{
    LOG_DEBUG << "Processing login";
    HttpViewData viewData;
    drogon::HttpResponsePtr response;
    std::string errorMessage;
    std::string encryptedPassword;
    SHA256 cripto;
    const drogon::SessionPtr SESSION = request->session();

    std::string username = request->getParameter("username");
    std::string userIdStr;
    std::string password = request->getParameter("password");
    std::string hashFromDatabase;

    trantor::Date now = trantor::Date::date();
    viewData.insert("date", now.toCustomedFormattedStringLocal("%d-%m-%Y as %H:%M:%S", false));
    viewData.insert("cpyright", Common::GetCpyRightMessage());

    // Checar se o banco de dados já está aberto
    if (!dbm){
        // Caso esteja fechado, abri-lo
        dbm = drogon::orm::DbClient::newPgClient(Common::GetDatabaseConnectionCredentials(), 2);
    }

    // Obter todos os usuario com o mesmo nome a ser inserido
    drogon::orm::Result results = dbm->execSqlSync("SELECT * FROM users_credentials WHERE username = $1", username);
    if (results.empty()) {
        // Usuario ainda nao existe no DB
        // Checar se as senhas conferem.
        errorMessage = "Usuario nao existe!";
        viewData.insert("errorMessage", errorMessage);

        response = HttpResponse::newHttpViewResponse("loginUnsuccessfull", viewData);
    } else {
        // Usuário já existe no DB
        encryptedPassword = cripto(password);
        hashFromDatabase = results[0][2].c_str();
        if (encryptedPassword == hashFromDatabase) {
            // Senha correta
            userIdStr = results[0][0].c_str();
            SESSION->insert(USER_ID, (int)std::stoul(userIdStr));
            response = HttpResponse::newHttpViewResponse("sucessLoginpage", viewData);
        } else {
            // Senha incorreta
            errorMessage = "Senha incorreta!";
            viewData.insert("errorMessage", errorMessage);

            response = HttpResponse::newHttpViewResponse("loginUnsuccessfull", viewData);
        }
    }

    callback(response);
}

void
Login::ProcessLogout(const HttpRequestPtr& request,std::function<void (const HttpResponsePtr &)> &&callback) const
{
    LOG_DEBUG << "Processing logout";
    HttpViewData viewData;
    drogon::HttpResponsePtr response;
    std::string errorMessage;
    const drogon::SessionPtr SESSION = request->session();

    if (SESSION->find(USER_ID)) {
        LOG_DEBUG << "logout successfull!";
        // Usuário logado
        SESSION->erase(USER_ID); // Remover o cookie do id do usuario
        response = HttpResponse::newHttpViewResponse("logout", viewData);
    } else {
        LOG_DEBUG << "can't log out inexistent user";
        // Usuário não logado
        response = HttpResponse::newHttpViewResponse("logoutUnsuccessfull", viewData);
    }

    callback(response);
}