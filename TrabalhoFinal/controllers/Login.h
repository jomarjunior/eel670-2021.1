/**
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * Escola Politécnica
 * Departamento de Eletrônica e Computação - DEL
 * 2021.1
 * Linguagens de Programação
 * Professor: Miguel Elias Mitre Campista
 * Alunos:  Bernardo B. Dias
 *          Jomar Júnior de Souza Pereira
 **/

#pragma once
#include <drogon/HttpController.h>
using namespace drogon;
class Login:public drogon::HttpController<Login>
{
  public:
    METHOD_LIST_BEGIN
    //use METHOD_ADD to add your custom processing function here;
    //METHOD_ADD(Login::get,"/{2}/{1}",Get);//path is /Login/{arg2}/{arg1}
    //METHOD_ADD(Login::your_method_name,"/{1}/{2}/list",Get);//path is /Login/{arg1}/{arg2}/list
    ADD_METHOD_TO(Login::RenderLoginPage,"/login",Get);
    ADD_METHOD_TO(Login::ProcessLogin,"/login",Post);
    ADD_METHOD_TO(Login::ProcessLogout,"/logout",Get);

    METHOD_LIST_END
    // your declaration of processing function maybe like this:
    // void get(const HttpRequestPtr& req,std::function<void (const HttpResponsePtr &)> &&callback,int p1,std::string p2);
    void RenderLoginPage(const HttpRequestPtr& request,std::function<void (const HttpResponsePtr &)> &&callback) const;
    void ProcessLogin(const HttpRequestPtr& request,std::function<void (const HttpResponsePtr &)> &&callback) const;
    void ProcessLogout(const HttpRequestPtr& request,std::function<void (const HttpResponsePtr &)> &&callback) const;
};
