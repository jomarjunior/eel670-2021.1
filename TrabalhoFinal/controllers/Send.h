/**
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * Escola Politécnica
 * Departamento de Eletrônica e Computação - DEL
 * 2021.1
 * Linguagens de Programação
 * Professor: Miguel Elias Mitre Campista
 * Alunos:  Bernardo B. Dias
 *          Jomar Júnior de Souza Pereira
 **/

#pragma once
#include <drogon/HttpController.h>
using namespace drogon;
class Send:public drogon::HttpController<Send>
{
  public:
    METHOD_LIST_BEGIN
    ADD_METHOD_TO(Send::SendPost,"/send",Post);

    METHOD_LIST_END
    // your declaration of processing function maybe like this:
    // void get(const HttpRequestPtr& req,std::function<void (const HttpResponsePtr &)> &&callback,int p1,std::string p2);
    void SendPost(const HttpRequestPtr& request,std::function<void (const HttpResponsePtr &)> &&callback) const;
};
