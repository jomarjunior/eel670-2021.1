/**
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * Escola Politécnica
 * Departamento de Eletrônica e Computação - DEL
 * 2021.1
 * Linguagens de Programação
 * Professor: Miguel Elias Mitre Campista
 * Alunos:  Bernardo B. Dias
 *          Jomar Júnior de Souza Pereira
 **/

#pragma once
#include <drogon/HttpController.h>
using namespace drogon;
class Register:public drogon::HttpController<Register>
{
  public:
    METHOD_LIST_BEGIN

    ADD_METHOD_TO(Register::RenderRegisterPage,"/register",Get); // Renderizar a pagina de registro com views
    ADD_METHOD_TO(Register::RegisterUser,"/register",Post); // Registrar um usuario no banco de dados

    METHOD_LIST_END
    void RenderRegisterPage(const HttpRequestPtr& request,std::function<void (const HttpResponsePtr &)> &&callback) const;
    void RegisterUser(const HttpRequestPtr& request,std::function<void (const HttpResponsePtr &)> &&callback) const;
};
