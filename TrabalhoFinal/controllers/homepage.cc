/**
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * Escola Politécnica
 * Departamento de Eletrônica e Computação - DEL
 * 2021.1
 * Linguagens de Programação
 * Professor: Miguel Elias Mitre Campista
 * Alunos:  Bernardo B. Dias
 *          Jomar Júnior de Souza Pereira
 **/

#include "homepage.h"
#include "../models/Common.h"
//add definition of your processing function here

#define USER_ID         "uid"
#define IS_LOGGED_IN    "isLoggedIn"

extern drogon::orm::DbClientPtr dbm;

void
homepage::RenderHomePage ( const HttpRequestPtr &request, std::function<void (const HttpResponsePtr &)> &&callback )
{
    LOG_DEBUG << "Rendering homepage...\n";
    int userId = -1;
    HttpViewData viewData;
    std::vector<std::vector<std::string>> posts;

    const drogon::SessionPtr SESSION = request->session();

    trantor::Date now = trantor::Date::date();
    viewData.insert("date", now.toCustomedFormattedStringLocal("%d-%m-%Y as %H:%M:%S", false));
    viewData.insert("cpyright", Common::GetCpyRightMessage());

    if (SESSION->find(USER_ID)) {
        // Usuário logado
        userId = SESSION->get<int>(USER_ID);
        viewData.insert(IS_LOGGED_IN, true);

        // Checar se o banco de dados já está aberto
        if (!dbm){
            // Caso esteja fechado, abri-lo
            dbm = drogon::orm::DbClient::newPgClient(Common::GetDatabaseConnectionCredentials(), 1);
        }

        LOG_DEBUG << userId;
        drogon::orm::Result results = dbm->execSqlSync("SELECT USERNAME, ID FROM users_credentials");
        for (auto result : results) {
            std::string tempUsername = result[0].c_str();
            int tempUid = (int) std::stoul(result[1].c_str());
            if (tempUid == userId) {
                viewData.insert("username", tempUsername);
            }
        }
        //viewData.insert("username", results[0][0].c_str());
        results = dbm->execSqlSync(R"(SELECT B.USERNAME, A.MESSAGE, A.ID, A.UID
                                      FROM USERS_POSTS A
                                      JOIN USERS_CREDENTIALS B ON A.UID = B.ID)");
        for (int index = results.size() - 1; index >= 0; index--) {
            std::vector<std::string> tempPost;
            tempPost.push_back(results[index][0].c_str());
            tempPost.push_back(results[index][1].c_str());
            tempPost.push_back(results[index][2].c_str());
            tempPost.push_back(results[index][3].c_str());

            posts.push_back(tempPost);
        }
        viewData.insert("posts", posts);
    } else {
        // Usuário não logado
        viewData.insert(IS_LOGGED_IN, false);
    }
    viewData.insert(USER_ID, userId);

    auto response = HttpResponse::newHttpViewResponse("homepage", viewData);
    LOG_DEBUG << "Homepage endend, calling callback...\n";
    callback (response);
}

void
homepage::DeletePost ( const HttpRequestPtr &request, std::function<void (const HttpResponsePtr &)> &&callback )
{
    LOG_DEBUG << "Processing post deletion";
    int uid = -1;
    std::string postUid;
    std::string postId;

    const drogon::SessionPtr SESSION = request->session();
    HttpResponsePtr response;

    if (SESSION->find(USER_ID)) {
        // Usuário logado
        uid = SESSION->get<int>(USER_ID);
        postId = request->getParameter("postId");
        postUid = request->getParameter("postUid");


        if (uid == std::stoul(postUid)) {
            // post com mesmo id de quem esta tentando deletar
            // Checar se o banco de dados já está aberto
            if (!dbm){
                // Caso esteja fechado, abri-lo
                dbm = drogon::orm::DbClient::newPgClient(Common::GetDatabaseConnectionCredentials(), 1);
            }
            // Criar a Query
            std::string query = "DELETE FROM USERS_POSTS WHERE ID = ";
            query = query + postId;

            drogon::orm::Result results = dbm->execSqlSync(query);
            response = HttpResponse::newHttpViewResponse("successfullDelete");
        } else {
            response = HttpResponse::newHttpViewResponse("failureDelete");
        }
    } else {
        // Usuário não logado
        response = HttpResponse::newHttpViewResponse("failureDelete");
    }

    callback(response);
}