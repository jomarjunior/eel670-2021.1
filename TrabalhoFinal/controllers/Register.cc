/**
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * Escola Politécnica
 * Departamento de Eletrônica e Computação - DEL
 * 2021.1
 * Linguagens de Programação
 * Professor: Miguel Elias Mitre Campista
 * Alunos:  Bernardo B. Dias
 *          Jomar Júnior de Souza Pereira
 **/

#include "Register.h"
#include "../models/Common.h"
#include "../models/sha256.h"
#include <drogon/orm/DbClient.h>

//add definition of your processing function here

#define USER_ID         "uid"

extern drogon::orm::DbClientPtr dbm;

void Register::RenderRegisterPage(const HttpRequestPtr& request,std::function<void (const HttpResponsePtr &)> &&callback) const
{
    LOG_DEBUG << "Rendering register page\n";
    const drogon::SessionPtr SESSION = request->session();

    HttpViewData viewData;

    trantor::Date now = trantor::Date::date();
    viewData.insert("date", now.toCustomedFormattedStringLocal("%d-%m-%Y as %H:%M:%S", false));
    viewData.insert("cpyright", Common::GetCpyRightMessage());

    // Proibir o usuário de se registrar caso esteja logado
    if (SESSION->find(USER_ID)) {
        // Usuário logado
        LOG_DEBUG << "User already logged in can't register.\n";
        auto response = HttpResponse::newHttpViewResponse("registerLoggedError", viewData);
        callback(response);
    } else {
        // Usuário não logado
        LOG_DEBUG << "User not logged in, proceding to register page.\n";
        auto response = HttpResponse::newHttpViewResponse("registerpage", viewData);
        callback(response);
    }
}

void Register::RegisterUser(const HttpRequestPtr& request,std::function<void (const HttpResponsePtr &)> &&callback) const
{
    LOG_DEBUG << "Rendering register request on user: " << request->getParameter("username");
    HttpViewData viewData;
    SHA256 cripto;
    drogon::HttpResponsePtr response;

    std::string username = request->getParameter("username");
    std::pair<std::string, std::string> password = std::make_pair(request->getParameter("password"), request->getParameter("password2"));
    std::string encriptedPassword;

    std::string errorMessage;

    trantor::Date now = trantor::Date::date();
    viewData.insert("date", now.toCustomedFormattedStringLocal("%d-%m-%Y as %H:%M:%S", false));
    viewData.insert("cpyright", Common::GetCpyRightMessage());

    // Checar se o banco de dados já está aberto
    if (!dbm){
        // Caso esteja fechado, abri-lo
        dbm = drogon::orm::DbClient::newPgClient(Common::GetDatabaseConnectionCredentials(), 1);
    }

    // Obter todos os usuario com o mesmo nome a ser inserido
    drogon::orm::Result results = dbm->execSqlSync("SELECT * FROM users_credentials WHERE username = $1", username);
    if (results.empty()) {
        // Usuario ainda nao existe no DB
        // Checar se as senhas conferem.
        if (password.first != password.second) {
            // As senhas nao conferem
            errorMessage = "As senhas devem ser idênticas.";
            viewData.insert("errorMessage", errorMessage);
            viewData.insert("username", username);
            viewData.insert("password", "");
            viewData.insert("password2", "");

            response = HttpResponse::newHttpViewResponse("registerpageUnsuccessfull", viewData);
        } else {
            // As senhas conferem
            encriptedPassword = cripto(password.first);
            LOG_DEBUG << encriptedPassword;
            results = dbm->execSqlSync("INSERT INTO users_credentials (ID, USERNAME, PASSWORD) VALUES (nextval('seq_users_credentials'), $1, $2)", username, encriptedPassword);
            
            viewData.insert("username", username);
            response = HttpResponse::newHttpViewResponse("registerpageSuccessfull", viewData);
        }
    } else {
        // Usuário já existe no DB
        errorMessage = "O nome de usuario ja existe.";
        viewData.insert("errorMessage", errorMessage);
        viewData.insert("username", "");
        viewData.insert("password", "");
        viewData.insert("password2", "");
        response = HttpResponse::newHttpViewResponse("registerpageUnsuccessfull", viewData);
    }

    callback(response);
}
