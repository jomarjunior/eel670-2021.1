/**
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * Escola Politécnica
 * Departamento de Eletrônica e Computação - DEL
 * 2021.1
 * Linguagens de Programação
 * Professor: Miguel Elias Mitre Campista
 * Alunos:  Bernardo B. Dias
 *          Jomar Júnior de Souza Pereira
 **/

#pragma once
#include <drogon/HttpController.h>

using namespace drogon;

class homepage:public drogon::HttpController<homepage>
{
  public:
    METHOD_LIST_BEGIN
    //use METHOD_ADD to add your custom processing function here;
      ADD_METHOD_TO(homepage::RenderHomePage, "/", Get);
      ADD_METHOD_TO(homepage::DeletePost, "/", Post);
    METHOD_LIST_END
    // your declaration of processing function maybe like this:
    void RenderHomePage ( const HttpRequestPtr &request, std::function<void (const HttpResponsePtr &)> &&callback );
    void DeletePost ( const HttpRequestPtr &request, std::function<void (const HttpResponsePtr &)> &&callback );
};
