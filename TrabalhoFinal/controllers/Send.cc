/**
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * Escola Politécnica
 * Departamento de Eletrônica e Computação - DEL
 * 2021.1
 * Linguagens de Programação
 * Professor: Miguel Elias Mitre Campista
 * Alunos:  Bernardo B. Dias
 *          Jomar Júnior de Souza Pereira
 **/

#include "Send.h"
#include "../models/Common.h"
#include <drogon/orm/DbClient.h>
//add definition of your processing function here


extern drogon::orm::DbClientPtr dbm;

void
Send::SendPost(const HttpRequestPtr& request,std::function<void (const HttpResponsePtr &)> &&callback) const
{
    LOG_DEBUG << "Processing send post";
    drogon::HttpResponsePtr response;
    HttpViewData viewData;
    int uid = (int) std::stoul(request->getParameter("uid"));
    std::string message = request->getParameter("userPost");

    if (message.size() > 140) {
        return;
    }

    // Checar se o banco de dados já está aberto
    if (!dbm){
        // Caso esteja fechado, abri-lo
        dbm = drogon::orm::DbClient::newPgClient(Common::GetDatabaseConnectionCredentials(), 1);
    }

    LOG_DEBUG << uid;
    // Unico jeito que consegui fazer funcionar
    std::string query = "INSERT INTO users_posts (ID, UID, MESSAGE) VALUES (nextval('SEQ_USERS_POSTS'), ";
    query = query + std::to_string(uid);
    query = query + ", '";
    query = query + message;
    query = query + "')";

    drogon::orm::Result results = dbm->execSqlSync(query);

    response = HttpResponse::newHttpViewResponse("sendSuccessfull", viewData);
    callback(response);
}