/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 17 DE SETEMBRO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#include <iostream>
#include <string>
#include <vector>


#include "include/error.h"
#include "include/configurations.h"
#include "include/Catalog.h"
#include "include/Messenger.h"
#include "include/movie.h"
#include "include/FileHandler.h"
#include "include/Populator.h"

std::istream&
operator>> (std::istream &in, movie::movie &movie)
{
    std::string scoreStr;
    std::cout << "Entre com o nome do filme" << std::endl;
    std::getline(in, movie.name);
    std::cout << "Entre com o nome da produtora do filme" << std::endl;
    std::getline(in, movie.producer);
    std::cout << "Entre com a pontuacao do filme" << std::endl;
    std::getline(in, scoreStr);
    movie.score = std::stod(scoreStr);
    return in;
}

int
main (int argc, char *argv[])
{
    FileHandler *fileHandler = new FileHandler ("../dados/catalogo.txt");
    Catalog *catalog = new Catalog (configurations::maximumAmountOfMovies);
    Populator *populator = new Populator (catalog);
    std::string auxiliarString = "";
    movie::movie auxiliarMovie;
    movie::movie *ptrMovie;
    Messenger *messenger = new Messenger;
    std::vector<std::pair<std::string, std::vector<std::string>>> fileContent, fileSaveContent;
    std::vector<movie::movie> movieList;

    char option = ' ';
    int optionIndex = 0;

    errorType error = error::ok;
    
    // Checar a quantidade de argumentos
    if (argc > configurations::highestAmountOfArguments || argc < configurations::leastAmountOfArguments){
        messenger->ShowErrorMessage (error::notEnoughArguments);
        messenger->ShowHelpMessage();
        delete messenger;
        delete catalog;
        exit(error::notEnoughArguments);
    }

    if ((error = fileHandler->Open()) != error::ok) {
        messenger->ShowErrorMessage (error);
        delete messenger;
        delete catalog;
        delete fileHandler;
        exit (error);
    }
    
    fileHandler->Read(&fileContent);
    
    fileHandler->Close();

    // Popular
    if((error = populator->Populate(fileContent)) != error::ok) {
        messenger->ShowErrorMessage (error);
        delete messenger;
        delete catalog;
        delete fileHandler;
        exit (error);
    }

    // Ler os argumentos de linha de comando e obter as long_options ou short_options usadas.
    while ((option = getopt_long(argc, argv, configurations::shortOptions.c_str(), configurations::longOptions, &optionIndex)) > -1) {
        switch (option) {
            case 'i':
                std::cin >> auxiliarMovie;
                if ((error = *catalog += auxiliarMovie) != error::ok) {
                    messenger->ShowErrorMessage (error);
                    delete messenger;
                    delete catalog;
                    delete fileHandler;
                    exit(error);
                }
                fileSaveContent = populator->AntiPopulate(catalog);
                fileHandler->Save(fileSaveContent);
                messenger->ShowInsertedIntoCatalog (auxiliarMovie);
                break;
            case 'r':
                std::cout << "Insira o nome do filme a ser removido: " << std::endl;
                std::getline(std::cin, auxiliarMovie.name);
                if ((error = *catalog -= auxiliarMovie) != error::ok) {
                    messenger->ShowErrorMessage (error);
                    delete messenger;
                    delete catalog;
                    delete fileHandler;
                    exit(error);
                }
                fileSaveContent = populator->AntiPopulate(catalog);
                fileHandler->Save(fileSaveContent);
                messenger->ShowRemovedFromCatalog (auxiliarMovie);
                break;
            case 's':
                std::cout << "Insira o nome do filme a ser encontrado: " << std::endl;
                std::getline(std::cin, auxiliarMovie.name);
                messenger->ShowFoundMovie((*catalog)(auxiliarMovie.name));
                break;
            case 'N':
                std::cout << "Insira o nome do filme a ser editado: " << std::endl;
                std::getline(std::cin, auxiliarString);
                std::cout << "Insira os novos dados do filme" << std::endl;
                std::cin >> auxiliarMovie;
                ptrMovie = (*catalog)(auxiliarString, auxiliarMovie);
                fileSaveContent = populator->AntiPopulate(catalog);
                fileHandler->Save(fileSaveContent);
                messenger->ShowEditedMovieNameCatalog (ptrMovie, auxiliarString);
                break;
            case 'P':
                std::cout << "Insira o nome do filme a ser editado: " << std::endl;
                std::getline(std::cin, auxiliarString);
                std::cout << "Insira o novo produtor do filme" << std::endl;
                std::getline(std::cin, auxiliarMovie.producer);
                ptrMovie = (*catalog)(auxiliarString, auxiliarMovie.producer);
                fileSaveContent = populator->AntiPopulate(catalog);
                fileHandler->Save(fileSaveContent);
                messenger->ShowEditedMovieProducerCatalog (ptrMovie);
                break;
            case 'S':
                std::cout << "Insira o nome do filme a ser editado: " << std::endl;
                std::getline(std::cin, auxiliarMovie.name);
                std::cout << "Insira a nova pontuacao do filme" << std::endl;
                std::getline(std::cin, auxiliarString);
                auxiliarMovie.score = std::stod (auxiliarString);
                ptrMovie = (*catalog)(auxiliarMovie.name, auxiliarMovie.score);
                fileSaveContent = populator->AntiPopulate(catalog);
                fileHandler->Save(fileSaveContent);
                messenger->ShowEditedMovieScoreCatalog (ptrMovie);
                break;
            case 'p':
                messenger->ShowFullCatalog(catalog);
                break;
            case 'H':
                messenger->ShowHighestScoreMovie(catalog);
                break;
            // Ajuda
            case 'h':
                messenger->ShowHelpMessage();
            break;
            // Opção errada.
            default:
                messenger->ShowErrorMessage (error::wrongOption);
                messenger->ShowHelpMessage();
            break;
        }
    }

    

    delete messenger;
    delete catalog;
    delete fileHandler;
    return error::ok;
}