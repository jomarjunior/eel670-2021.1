/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 17 DE SETEMBRO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#include "Messenger.h"

#include <iostream>
#include <string>
#include <vector>

#include "configurations.h"
#include "Catalog.h"
#include "movie.h"
#include "error.h"

std::ostream&
operator<< (std::ostream &os, movie::movieStruct &movie)
{
    os << movie.name << " :: " << movie.producer << " :: " << movie.score << std::endl;
    return os;
}

std::ostream&
operator<< (std::ostream &os, Catalog &catalog)
{
    for (movie::movie movie : catalog.GetMovieList()) {
        os << movie;
    }
    return os;
}

/**
 * Exibe na tela a mensagem relacionada a um erro de execução.
 * @param error Erro encontrado.
 */
void
Messenger::ShowErrorMessage ( errorType error )
{
    std::cout << "---- Erro detectado! ----" << std::endl;
    std::cout << "Codigo " << error << " - " << error::errorMessage[error] << std::endl;
    std::cout << "-------------------------" << std::endl;
}

/*
 * Exibe na tela uma mensagem contendo todos os comandos aceitos pelo programa.
 */
void
Messenger::ShowHelpMessage ()
{
    std::cout << "Comandos aceitos: \n\n";
    std::cout << "--insert / -i\n\tInsere um filme no catalogo. Um dialogo sera iniciado.\n\tUso: --insert";
    std::cout << std::endl;
    std::cout << "--remove / -r\n\tRemove um filme do catalogo. Um dialogo sera iniciado.\n\tUso: --remove";
    std::cout << std::endl;
    std::cout << "--search / -s\n\tProcura por um nome de filme no catalogo. Um dialogo sera iniciado.\n\tUso: --search";
    std::cout << std::endl;
    std::cout << "--editName / -N\n\tAltera o nome de um filme do catalogo. Um dialogo sera iniciado.\n\tUso: --editName";
    std::cout << std::endl;
    std::cout << "--editProducer / -P\n\tAltera o produtor de um dado filme do catalogo. Um dialogo sera iniciado.\n\tUso: --editProducer";
    std::cout << std::endl;
    std::cout << "--editScore / -S\n\tAltera a pontuacao de um dado filme do catalogo. Um dialogo sera iniciado.\n\tUso: --editScore";
    std::cout << std::endl;
    std::cout << "--print / -p\n\tImprime na tela todos os filmes do catalogo com suas produtoras e pontuacao.\n\tUso: --print";
    std::cout << std::endl;
    std::cout << "--highestscore / -H\n\tImprime na tela o filme melhor avaliado do catalogo.\n\tUso: --highestscore";
    std::cout << std::endl;
    std::cout << "--help / -i\n\tImprime na tela esta lista de ajuda.\n\tUso: --help";
    std::cout << std::endl;
    
}

void
Messenger::ShowInsertedIntoCatalog (movie::movie movie)
{
    std::cout << "O filme " << movie.name << " da produtora " << movie.producer << " foi inserido no catalogo com sucesso!" << std::endl;
}

void
Messenger::ShowRemovedFromCatalog (movie::movie movie)
{
    std::cout << "O filme " << movie.name << " foi removido do catalogo com sucesso!" << std::endl;
}

void
Messenger::ShowFoundMovie (movie::movie *movie)
{
    if (!movie)
        std::cout << "Filme nao encontrado!" << std::endl;
    else {
        std::cout << "Filme encontrado:" << std::endl;
        std::cout << "Nome :: Produtora :: Pontuacao" << std::endl;
        std::cout << *movie << std::endl;
    }
}

void
Messenger::ShowEditedMovieNameCatalog (movie::movie *movie, std::string removedMovieName)
{
    if (!movie) std::cout << "Filme nao encontrado!" << std::endl;
    else std::cout << "Filme editado no catalogo.\n Removido: " << removedMovieName << "\n Adicionado: " << movie->name << std::endl;
}

void
Messenger::ShowEditedMovieProducerCatalog (movie::movie *movie)
{
    if (!movie) std::cout << "Filme nao encontrado" << std::endl;
    else std::cout << "Filme " << movie->name << " editado no catalogo. Novo produtor: " << movie->producer << std::endl;
}

void
Messenger::ShowEditedMovieScoreCatalog (movie::movie *movie)
{
    if (!movie) std::cout << "Filme nao encontrado!" << std::endl;
    else std::cout << "Filme " << movie->name << " editado no catalogo. Nova pontuacao: " << movie->score << std::endl;
}

void
Messenger::ShowFullCatalog (Catalog *catalog)
{
    if (! catalog) std::cout << "Catalogo nao fornecido!" << std::endl;
    else std::cout << *catalog << std::endl;
}

void
Messenger::ShowHighestScoreMovie (Catalog *catalog)
{
    if (! catalog) std::cout << "Catalogo nao fornecido!" << std::endl;
    else std::cout << "Filme com maior pontuacao: " << *(catalog->GetHighestScoreMovie()) << " Pontuacao: " << catalog->GetHighestScoreMovie()->score << std::endl;
}