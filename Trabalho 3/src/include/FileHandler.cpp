/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 17 DE SETEMBRO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#include "FileHandler.h"

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <sstream>
#include <utility>
#include <cstdio>
#include <stdlib.h>
#include <stdio.h>

#include "error.h"

/**
 * Construtor do controlador de arquivos, recebe o arquivo a ser lido.
 * @param file arquivo a ser lido com seu caminho relativo ou completo.
 */
FileHandler::FileHandler (std::string fileName, char sepparator)
{
    this->fileName = fileName;
    this->sepparator = sepparator;
}

/**
 * Abre o arquivo associado ao controlador de arquivos.
 * @return O código de erro associado ao problema encontrado ou error::ok caso não haja.
 */
errorType
FileHandler::Open ()
{
    if (this->fileName.empty()) return error::noFileName;
    if (this->file) return error::fileAlreadyOpen;

    this->file = new std::ifstream (this->fileName);
    
    if (!this->file->is_open()) return error::couldNotOpenFile;

    return error::ok;
}

/**
 * Fecha o arquivo aberto anteriormente pela FileHandler::Open();
 * @return O código de erro associado ao problema encontrado ou error::ok caso não haja.
 */
errorType
FileHandler::Close ()
{
    if (!this->file) return error::nullFile;
    if (!this->file->is_open()) return error::fileAlreadyClosed;

    this->file->close();

    if (this->file->is_open()) return error::couldNotCloseFile;

    return error::ok;
}

/**
 * Lê o arquivo anteriormente aberto e devolve um vetor de pares.
 * @param result <saída> vetor de pares com a coluna seguida de um vetor de seus valores em string.
 * @param haveHeader indica a existência ou não de um cabeçalho no arquivo (primeira linha não contém dados, mas o nome das colunas)
 * @return O código de erro associado ao problema encontrado ou error::ok caso não haja. 
 */
errorType
FileHandler::Read (std::vector<std::pair<std::string, std::vector<std::string>>> *result, bool haveHeader)
{
    if (!this->file->is_open()) return error::fileIsNotOpen;
    if (!this->file->good()) return error::badFileStream;

    std::string line;
    std::string columnName;
    
    if (haveHeader) {
        getline(*this->file, line);

        std::stringstream stringStream (line);
        while (getline(stringStream, columnName, this->sepparator)) {
            result->push_back({columnName, std::vector<std::string>{}});
        }
    }

    while (getline(*this->file, line)) {
        std::stringstream stringStream (line);

        std::string value;
        unsigned int valueIndex = 0;
        while (getline(stringStream, value, this->sepparator)) {
            if (valueIndex + 1 > result->size()) {
                result->push_back({"", std::vector<std::string>{}});
            }
            result->at(valueIndex).second.push_back(value);
            valueIndex++;
        }
    }
    return error::ok;
}

/**
 * Salva o vetor de colunas no arquivo fornecido no construtor da classe. O salvamento é feito sobrescrevendo o arquivo atual com um novo.
 * @param content Conteudo do novo arquivo
 * @param haveHeader Caso haja a existencia de cabeçalhos no arquivo.
 * @return codigo de erro correspondente ou error::ok
 */
errorType
FileHandler::Save (std::vector<std::pair<std::string, std::vector<std::string>>> content, bool haveHeader)
{
    if (this->file->is_open()) return error::fileAlreadyOpen;

    std::string line;
    std::string columnName;
    char tempName[27] = "../dados/catalog.txtXXXXXX";
    FILE *tempFile = fdopen(mkstemp(tempName), "w");
    
    if (haveHeader) {
        fputs((content.at(0).first).c_str(), tempFile);
        fputs(",", tempFile);
        fputs((content.at(1).first).c_str(), tempFile);
        fputs(",", tempFile);
        fputs((content.at(2).first).c_str(), tempFile);
        fputs("\n", tempFile);
    }

    for (unsigned int index = 0; index < content.at(0).second.size(); index++) {
        fputs(content.at(0).second.at(index).c_str(), tempFile);
        fputs(",", tempFile);
        fputs(content.at(1).second.at(index).c_str(), tempFile);
        fputs(",", tempFile);
        fputs(content.at(2).second.at(index).c_str(), tempFile);
        if (index + 1 < content.at(0).second.size())
            fputs("\n", tempFile);
    }

    remove(this->fileName.c_str());
    rename(tempName, this->fileName.c_str());
    fclose (tempFile);
    return error::ok;
}

/**
 * Destrutor da classe, responsável por checar se o arquivo está aberto e fecha-lo, além de liberar a memória dos seus ponteiros.
 */
FileHandler::~FileHandler ()
{
    if (this->file->is_open()) this->file->close();

    delete this->file;
}