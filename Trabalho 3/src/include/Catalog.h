/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 17 DE SETEMBRO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef CATALOG
#define CATALOG

#include <iostream>
#include <ostream>
#include <string>
#include <vector>

#include "movie.h"
#include "error.h"

class Catalog
{
    private:
    std::vector<movie::movie> movieList;
    unsigned long maximumMovieAmount;

    public:
    Catalog (int);

    errorType operator+= (movie::movie);
    errorType operator+= (std::vector<movie::movie>);
    errorType operator-= (movie::movie);
    movie::movie *operator() (const std::string);
    movie::movie *operator() (const std::string, movie::movie);
    movie::movie *operator() (const std::string, std::string);
    movie::movie *operator() (const std::string, double);
    friend std::ostream& operator<< (std::ostream &, const Catalog &);

    std::vector<movie::movie> GetMovieList ();
    movie::movie *GetHighestScoreMovie ();
};

std::ostream& operator<< (std::ostream &, const Catalog &);

#endif