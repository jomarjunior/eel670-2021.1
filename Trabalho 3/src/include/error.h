/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 17 DE SETEMBRO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef ERROR
#define ERROR

/*
 * Namespace que guarda as constantes de erro do programa.
 */
namespace error
{
    const unsigned int ok = 0;
    const unsigned int wrongOption = 1;
    const unsigned int emptyMovieList = 2;
    const unsigned int emptyMovieName = 3;
    const unsigned int emptyProducerName = 4;
    const unsigned int maximumNumberOfMovies = 5;
    const unsigned int movieAlreadyAdded = 6;
    const unsigned int notFoundMovieToErase = 7;
    const unsigned int notEnoughArguments = 8;
    const unsigned int noFileName = 9;
    const unsigned int fileAlreadyOpen = 10;
    const unsigned int couldNotOpenFile = 11;
    const unsigned int nullFile = 12;
    const unsigned int fileAlreadyClosed = 13;
    const unsigned int couldNotCloseFile = 14;
    const unsigned int fileIsNotOpen = 15;
    const unsigned int badFileStream = 16;
    const unsigned int nullCatalog = 17;
    const unsigned int invalidContent = 18;
    const unsigned int asymmetricContent = 19;

    const std::string errorMessage[20] = 
    {
        "Ok!",
        "Opção errada!",
        "Lista de filmes esta vazia!",
        "Nome do filme esta vazio!",
        "Nome do produtor do filme esta vazio!",
        "Numero maximo de filmes atingido!",
        "O filme em questao ja esta presente na lista de filmes.",
        "O filme nao consta na lista do catalogo para ser excluido.",
        "Quantidade de argumentos não e suficiente",
        "Nome do arquivo nao fornecido!",
        "Arquivo ja esta aberto!",
        "Nao foi possivel abrir o arquivo",
        "Arquivo nulo!",
        "Arquivo ja esta fechado!",
        "Nao foi possivel fechar o arquivo!",
        "Arquivo nao esta aberto!",
        "Fluxo de dados com problemas!",
        "Catalogo nulo",
        "Conteudo invalido",
        "Conteudo do arquivo de texto esta corrompido"
    };
}

typedef unsigned int errorType;

#endif