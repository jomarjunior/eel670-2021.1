/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 17 DE SETEMBRO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef POPULATOR
#define POPULATOR

#include <iostream>
#include <string>
#include <vector>
#include <utility>

#include "Catalog.h"

/**
 * Classe que popula um catalog utilizando um vetor de colunas e seus respectivos valores.
 */
class Populator
{
    private:
    Catalog *catalog;

    public:
    /**
     * Construtor da classe Populator, espera a entrada do catalog, já inicializado, que será populado.
     * @param catalog o catalog a ser alimentado.
     */
    Populator (Catalog *);

    /**
     * Método que executa o algoritmo de população no catalog fornecido no construtor da classe.
     * @param content vetor de pares de (<string> cabeçalho, <vetor de strings> valores da coluna).
     * @return código de erro equivalente ou error::ok caso sucesso.
     */
    errorType
    Populate (std::vector<std::pair<std::string, std::vector<std::string>>>);

    /**
     * Metodo que retorna um vetor de pares que representa as colunas de um arquivo csv pronto para ser salvo no disco.
     * @param catalog o catalogo que sera destrinchado.
     * @return código de erro equivalente ou error::ok caso sucesso.
     */
    std::vector<std::pair<std::string, std::vector<std::string>>>
    AntiPopulate (Catalog *);

};

#endif