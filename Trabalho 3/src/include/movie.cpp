/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 17 DE SETEMBRO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#include "movie.h"

#include <iostream>
#include <string>

bool
movie::movie::operator< (movie comparision)
{
    return this->name.compare(comparision.name) < 0;
}

bool
movie::movie::operator== (movie comparision)
{
    return this->name.compare(comparision.name) == 0;
}

bool
movie::movie::operator> (double refferenceScore)
{
    return this->score > refferenceScore;
}