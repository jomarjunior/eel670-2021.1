/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 17 DE SETEMBRO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef CONFIGURATIONS   
#define CONFIGURATIONS

#include <iostream>
#include <string>
#include <getopt.h>

/*
 * Namespace que guarda constantes de configuração do programa.
 */
namespace configurations
{
    // Main.cpp
    const struct option longOptions[] = {
        {"insert", no_argument, 0, 'i'},
        {"remove", no_argument, 0, 'r'},
        {"search", no_argument, 0, 's'},
        {"editName", no_argument, 0, 'N'},
        {"editProducer", no_argument, 0, 'P'},
        {"editScore", no_argument, 0, 'S'},
        {"print", no_argument, 0, 'p'},
        {"highestscore", no_argument, 0, 'H'},
        {"help", no_argument, 0, 'h'},
        {0, 0, 0, 0}
    };
    const std::string shortOptions = "irsNPSpHs";
    const int leastAmountOfArguments = 2;
    const int highestAmountOfArguments = 2;

    const int maximumAmountOfMovies = 10;
}

#endif