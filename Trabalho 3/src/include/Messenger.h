/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 17 DE SETEMBRO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef MESSENGER
#define MESSENGER

#include <iostream>
#include <string>

#include "error.h"
#include "Catalog.h"
#include "movie.h"

/**
 * Classe responsável por realizar impressões de texto e informações na tela.
 */
class Messenger
{
    private:

    public:

    /**
     * Exibe na tela a mensagem relacionada a um erro de execução.
     * @param error Erro encontrado.
     */
    void
    ShowErrorMessage(errorType);

    /*
    * Exibe na tela uma mensagem contendo todos os comandos aceitos pelo programa.
    */
    void
    ShowHelpMessage();

    void
    ShowInsertedIntoCatalog(movie::movie);

    void
    ShowRemovedFromCatalog(movie::movie);

    void
    ShowFoundMovie(movie::movie *);

    void
    ShowEditedMovieNameCatalog(movie::movie *, std::string);

    void
    ShowEditedMovieProducerCatalog(movie::movie *);

    void
    ShowEditedMovieScoreCatalog(movie::movie *);

    void
    ShowFullCatalog(Catalog *);

    void
    ShowHighestScoreMovie(Catalog *);
};

#endif