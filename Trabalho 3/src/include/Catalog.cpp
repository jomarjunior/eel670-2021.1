/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 17 DE SETEMBRO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#include "Catalog.h"

#include <iostream>
#include <ostream>
#include <string>
#include <vector>

#include "error.h"

Catalog::Catalog (int maximumMovieAmount)
{
    this->maximumMovieAmount = maximumMovieAmount;
}

errorType
Catalog::operator+= (movie::movie newMovie)
{
    std::vector<movie::movie>::iterator movieListIterator;

    if (this->movieList.size() + 1 > this->maximumMovieAmount) return error::maximumNumberOfMovies;

    if (newMovie.name.size() <= 0) return error::emptyMovieName;
    if (newMovie.producer.size() <= 0) return error::emptyProducerName;

    if (this->movieList.size() <= 0) {
        this->movieList.push_back(newMovie);
        return error::ok;
    }

    movieListIterator = this->movieList.begin();
    while ( movieListIterator != this->movieList.end() && ! (newMovie < *movieListIterator) ) {
        if (newMovie == *movieListIterator) return error::movieAlreadyAdded;
        movieListIterator++;
    }
    this->movieList.insert(movieListIterator, newMovie);

    return error::ok;
}

errorType
Catalog::operator+= (std::vector<movie::movie> newMovies)
{
    std::vector<movie::movie>::iterator movieListIterator;
    if (newMovies.size() <= 0) return error::emptyMovieList;
    if (this->movieList.size() + newMovies.size() > this->maximumMovieAmount) return error::maximumNumberOfMovies;

    for (movie::movie movie : newMovies) {
        if (movie.name.size() <= 0) return error::emptyMovieName;
        if (movie.producer.size() <= 0) return error::emptyProducerName;

        if (this->movieList.size() <= 0) {
            this->movieList.push_back(movie);
            continue;
        }
        movieListIterator = this->movieList.begin();
        while (movieListIterator != this->movieList.end() && !(movie < *movieListIterator)) {
            if (movie == *movieListIterator) break;
            movieListIterator++;
        }

        this->movieList.insert(movieListIterator, movie);
    }

    return error::ok;
}

errorType
Catalog::operator-= (movie::movie removeMovie)
{
    std::vector<movie::movie>::iterator movieListIterator;
    if (removeMovie.name.size() <= 0) return error::emptyMovieName;

    for (movieListIterator = this->movieList.begin(); movieListIterator != this->movieList.end(); movieListIterator++) {
        if (removeMovie == *movieListIterator) {
            this->movieList.erase(movieListIterator);
            return error::ok;
        }
    }

    return error::notFoundMovieToErase;
}

movie::movie *
Catalog::operator() (std::string movieName)
{
    movie::movie tempMovie;
    tempMovie.name = movieName;
    if (movieName.size() <= 0) return NULL;

    std::vector<movie::movie>::iterator movieListIterator;

    for (movieListIterator = this->movieList.begin(); movieListIterator != this->movieList.end(); movieListIterator++) {
        if (tempMovie == *movieListIterator) {
            return &(*movieListIterator);
        }
    }

    return NULL;
}

movie::movie *
Catalog::operator() (std::string movieName, movie::movie newMovie)
{
    movie::movie tempMovie, tempRemoveMovie;
    tempMovie.name = movieName;
    if (movieName.size() <= 0) return NULL;

    if ((*this)(newMovie.name)) return NULL;

    std::vector<movie::movie>::iterator movieListIterator;

    for (movieListIterator = this->movieList.begin(); movieListIterator != this->movieList.end(); movieListIterator++) {
        if (tempMovie == *movieListIterator) {
            tempRemoveMovie.name = movieListIterator->name;
            (*this) += newMovie;
            (*this) -= tempRemoveMovie;
            return (*this)(newMovie.name);
        }
    }

    return NULL;
}

movie::movie *
Catalog::operator() (std::string movieName, std::string producerName)
{
    movie::movie tempMovie;
    tempMovie.name = movieName;
    if (movieName.size() <= 0) return NULL;

    std::vector<movie::movie>::iterator movieListIterator;

    for (movieListIterator = this->movieList.begin(); movieListIterator != this->movieList.end(); movieListIterator++) {
        if (tempMovie == *movieListIterator) {
            movieListIterator->producer = producerName;
            return &(*movieListIterator);
        }
    }

    return NULL;
}

movie::movie *
Catalog::operator() (std::string movieName, double newScore)
{
    movie::movie tempMovie;
    tempMovie.name = movieName;
    if (movieName.size() <= 0) return NULL;

    std::vector<movie::movie>::iterator movieListIterator;

    for (movieListIterator = this->movieList.begin(); movieListIterator != this->movieList.end(); movieListIterator++) {
        if (tempMovie == *movieListIterator) {
            movieListIterator->score = newScore;
            return &(*movieListIterator);
        }
    }

    return NULL;
}

std::vector<movie::movie>
Catalog::GetMovieList ()
{
    return this->movieList;
}

movie::movie *
Catalog::GetHighestScoreMovie ()
{
    movie::movie *highestScoreMovie = new movie::movie;
    highestScoreMovie->name = "not found";
    highestScoreMovie->producer = "";
    highestScoreMovie->score = 0;
    if (this->movieList.size() <= 0) return highestScoreMovie;
    *highestScoreMovie = this->movieList.at(0);

    for (movie::movie movie : this->movieList) {
        if (movie.score > highestScoreMovie->score) {
            *highestScoreMovie = movie;
        }
    }

    return highestScoreMovie;
}