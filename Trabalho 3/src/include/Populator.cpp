/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 17 DE SETEMBRO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#include "Populator.h"

#include <iostream>
#include <string>
#include <vector>
#include <utility>

#include "Catalog.h"
#include "movie.h"
#include "error.h"

/**
 * Construtor da classe Populator, espera a entrada do catalog, já inicializado, que será populado.
 * @param catalog o catalog a ser alimentado.
 */
Populator::Populator (Catalog *catalog)
{
    this->catalog = catalog;
}

/**
 * Método que executa o algoritmo de população no catalog fornecido no construtor da classe.
 * @param content vetor de pares de (<string> cabeçalho, <vetor de strings> valores da coluna).
 * @return código de erro equivalente ou error::ok caso sucesso.
 */
errorType
Populator::Populate (std::vector<std::pair<std::string, std::vector<std::string>>> content)
{
    errorType error = error::ok;
    movie::movie auxiliarMovie;
    std::vector<movie::movie> movieListToAdd;

    if (!this->catalog) return error::nullCatalog;
    if (content.size() != 3) return error::invalidContent;
    if ((content.at(0).second.size() != content.at(1).second.size()) || (content.at(0).second.size() != content.at(2).second.size())) return error::asymmetricContent;

    for (unsigned int index = 0; index < content.at(0).second.size(); index++) {
        double score = std::stod(content.at(2).second.at(index));
        auxiliarMovie.name = content.at(0).second.at(index);
        auxiliarMovie.producer = content.at(1).second.at(index);
        auxiliarMovie.score = score;
        movieListToAdd.push_back(auxiliarMovie);
    }

    if (((*this->catalog) += movieListToAdd) != error::ok) return error;

    return error::ok;
}

/**
 * Metodo que retorna um vetor de pares que representa as colunas de um arquivo csv pronto para ser salvo no disco.
 * @param catalog o catalogo que sera destrinchado.
 * @return código de erro equivalente ou error::ok caso sucesso.
 */
std::vector<std::pair<std::string, std::vector<std::string>>>
Populator::AntiPopulate (Catalog *catalog)
{
    std::vector<std::pair<std::string, std::vector<std::string>>> result;

    std::vector<std::string> names;
    std::vector<std::string> producers;
    std::vector<std::string> scores;

    for (movie::movie movie : catalog->GetMovieList()) {
        names.push_back (movie.name);
        producers.push_back (movie.producer);
        scores.push_back (std::to_string(movie.score));
    }

    result.push_back(std::make_pair("", names));
    result.push_back(std::make_pair("", producers));
    result.push_back(std::make_pair("", scores));

    return result;
}