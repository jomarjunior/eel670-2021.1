/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 17 DE SETEMBRO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef MOVIE
#define MOVIE

#include <iostream>
#include <string>


namespace movie {
    typedef struct movieStruct {
        std::string name;
        std::string producer;
        double score;

        bool operator<(movieStruct);
        bool operator==(movieStruct);
        bool operator>(double); 
    } movie;
}

std::ostream& operator<< (std::ostream &, const movie::movieStruct &);
std::istream& operator>> (std::istream &, const movie::movieStruct &);

#endif