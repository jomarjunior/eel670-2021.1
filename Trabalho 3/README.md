Trabalho 2 - EEL670 - 2021.1
===
**UNIVERSIDADE FEDERAL DO RIO DE JANEIRO**

*Escola Politécnica*

*Departamento de Engenharia Eletrônica e de Computação*

Aluno: Jomar Júnior de Souza Pereira

O programa consiste num sistema de catalogo de filmes. Os filmes possuem um nome, uma produtora e uma pontuação associados. É possível inserir, remover, editar, imprimir e procurar filmes neste catalogo.

Estrutura
---
Foram implementadas 4 classes que atuam de forma distinta. São elas:

* **Catalog**

    Classe que implementa o catalogo e possui um vetor de filmes.

* **FileHandler**

    Classe que cuida da abertura e do fechamento do arquivo contendo as informações de construção do catalogo.

* **Messenger**

    Classe principal responsável por imprimir na tela as diversas informações do programa.

* **Populator**

    Classe responsável por popular o catalogo com os dados do arquivo lido anteriormente. Ela cria os filmes e alimenta o catalogo. Também é responsável por anti popular um catalogo para gerar o arquivo que será salvo após uma modificação.

Por fim, existem mais dois arquivos de cabeçalho que implementam namespaces:

* **configurations**

    Namespace que contém todas as constantes de configuração do programa, como opções de linha de comando, quantidade de argumentos esperada, etc...

* **error**

    Namespace que contém as constantes de erro que o programa retorna em diversos casos, além das mensagens relacionadas.

* **movie**

    Namespace que implementa a struct de um filme, com seus atributos e sobrecargas de operadores.

Todos esses arquivos devem estar dentro do diretório include/ que por sua vez deve estar no diretório src/, junto do arquivo **main.cpp** e do **makefile**.

Instruções de Compilação
---
A compilação do programa é de certa forma bem simples. São os arquivos necessários:

- main.cpp
- include/
    - Catalog.cpp
    - Catalog.h
    - configurations.h
    - error.h
    - FileHandler.cpp
    - FileHandler.h
    - Messenger.cpp
    - Messenger.h
    - movie.cpp
    - movie.h
    - Populator.cpp
    - Populator.h

Com esses arquivos, o compilador pode ser chamado da seguinte maneira após a criação dos objetos de linkedição de cada um dos **.cpp**:
```ubuntu
g++ -Wall -std=c++17 -o main.exe main.o Messenger.o FileHandler.o Populator.o Catalog.o movie.o
```

Instruções de Utilização
---
## Arquivo de dados
O programa faz a leitura de um arquivo **catalogo.txt** que deve estar localizado em "../dados/catalogo.txt" referente ao diretório do main.exe gerado.
O arquivo deve seguir o exemplo:
```
Shrek,Dreamworks,10
Wolverine Remastered,Marvel,19
CineGibi 6,Mauricio de Sousa Producoes,1
Procurando o Nemo,Pixar,13.78
A nova onda do imperador,Disney,1000
```

## Execução
A execução do programa espera uma opção de linha de comando para indicar qual a intenção do usuário. Exemplo de execução:
```ubuntu
./main.exe --insert
```
Lista de comandos:
```
--insert / -i
    Insere um filme no catalogo. Um dialogo sera iniciado.
    Uso: --insert
--remove / -r
    Remove um filme do catalogo. Um dialogo sera iniciado.
    Uso: --remove
--search / -s
    Procura por um nome de filme no catalogo. Um dialogo sera iniciado.
    Uso: --search
--editName / -N
    Altera o nome de um filme do catalogo. Um dialogo sera iniciado.
    Uso: --editName
--editProducer / -P
    Altera o produtor de um dado filme do catalogo. Um dialogo sera iniciado.
    Uso: --editProducer
--editScore / -S
    Altera a pontuacao de um dado filme do catalogo. Um dialogo sera iniciado.
    Uso: --editScore
--print / -p
    Imprime na tela todos os filmes do catalogo com suas produtoras e pontuacao.
    Uso: --print
--highestscore / -H
    Imprime na tela o filme melhor avaliado do catalogo.
    Uso: --highestscore
--help / -i
    Imprime na tela esta lista de ajuda.
    Uso: --help
```