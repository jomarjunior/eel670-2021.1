/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 04 DE OUTUBRO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef TREE_H
#define TREE_H

#include "Node.h"

#include <iostream>
#include <string>
#include <vector>

#include "error.h"

using namespace error;

template <typename contentType> class Tree;
template <typename contentType> std::ostream &operator<< (std::ostream &out, const Tree<contentType> &tree);

/**
 * Classe que implementa a Árvore Binária, utiliza a classe Node
 */
template <typename contentType>
class Tree
{
    private:
    Node<contentType> *root;
    unsigned int depth;

    public:
    /**
     * Construtor da classe Tree, configura a raíz e não recebe nenhum parâmetro.
     */
    Tree()
    {
        this->root = NULL;
        this->depth = 0;
    }

    /**
     * Operador de adição de uma folha à arvore. Recebe o conteúdo da folha, a cria e adiciona na árvore.
     * @param content conteúdo da folha.
     * @return Ponteiro para o dado adicionado à árvore ou NULL caso aconteça algum erro.
     */
    Node<contentType> *
    operator+= (contentType content)
    {
        Node<contentType> *newNode = new Node<contentType> (content);
        Node<contentType> *pointer = this->root;
        Node<contentType> *parent;
        unsigned int depthCounter = 0;

        // Caso a árvore esteja vazia
        if (!this->root){
            this->root = newNode;
            this->depth++;
        }
        // Caso a árvore não esteja vazia
        else {
            // Iterar até encontrar a folha da árvore
            while (pointer) {
                depthCounter++;
                // Checar o lado a seguir
                if (newNode->GetContent() > pointer->GetContent()) {
                    parent = pointer;
                    pointer = pointer->GetLeftNode();
                } else
                if (newNode->GetContent() < pointer->GetContent()){
                    parent = pointer;
                    pointer = pointer->GetRightNode();
                } else
                if (newNode->GetContent() == pointer->GetContent()){
                    return NULL;
                }
            }
            depthCounter++;
        
            // Checar se adiciona na direita ou na esquerda do nó
            if (newNode->GetContent() < parent->GetContent()){
                parent->SetRightNode(newNode);
            }
            else {
                parent->SetLeftNode(newNode);
            }

            // Atualizar a altura da ávore, caso necessário
            if (this->depth < depthCounter)
                this->depth = depthCounter;
        }

        // Retornar um ponteiro para o dado adicionado
        return newNode;
    }

    /**
     * Operador de busca de um nome na árvore. Percorre a árvore procurando pela string fornecida, tentando localiza-la no parâmetro 'nome' do conteúdo.
     * @param search Nome a ser procurado.
     * @return Ponteiro para o nó de mesmo nome ou NULL caso não encontre.
     */
    Node<contentType> *
    operator() (std::string search)
    {
        Node<contentType> *pointer = this->root;

        while (pointer) {
            if (pointer->GetContent() == search)                return pointer;
            if (pointer->GetContent() > search)                 pointer = pointer->GetRightNode();
            else if (pointer->GetContent() < search)            pointer = pointer->GetLeftNode();
        }

        return NULL;
    }

    /**
     * Operador que imprime na tela o conteúdo da ávore.
     */
    friend
    std::ostream &
    operator<< (std::ostream &out, Tree<contentType> &tree)
    {
        Tree<contentType> *leftSubTree = new Tree<contentType> ();
        Tree<contentType> *rightSubTree = new Tree<contentType> ();

        if (tree.GetRoot()){
            leftSubTree->SetRoot(tree.GetRoot()->GetRightNode());
            out << *leftSubTree;

            rightSubTree->SetRoot(tree.GetRoot()->GetLeftNode());
            out << *rightSubTree;

            out << *(tree.GetRoot()) << std::endl;
        }

        return out;
    }


    /**
     * Retorna a raíz da árvore binária
     * @return Nó de raíz da árvore.
     */
    Node<contentType> *
    GetRoot ()
    {
        return this->root;
    }

    /**
     * Define a raíz da árvore. Cuidado, a referência para os dados antigos poderá ser perdida.
     * @param newRootNode nova raíz.
     */
    errorType
    SetRoot (Node<contentType> *newRootNode)
    {
        this->root = newRootNode;
        return errorType::ok;
    }

    static
    std::vector<Node<contentType>>
    MakeVector ( Tree<contentType> *baseTree )
    {
        Tree<contentType> *leftSubTree = new Tree<contentType> ();
        Tree<contentType> *rightSubTree = new Tree<contentType> ();
        std::vector<Node<contentType>> result;

        if (!baseTree->GetRoot()) return result;

        result.push_back(*(baseTree->GetRoot()));

        leftSubTree->SetRoot(baseTree->GetRoot()->GetLeftNode());
        rightSubTree->SetRoot(baseTree->GetRoot()->GetRightNode());

        for (Node<contentType> node : MakeVector (leftSubTree))
            result.push_back (node);

        for (Node<contentType> node : MakeVector (rightSubTree))
            result.push_back (node);

        return result;

    }

};

#endif