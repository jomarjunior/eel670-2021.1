/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 04 DE OUTUBRO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef IN_PATIENT_H
#define IN_PATIENT_H

#include "Patient.h"

#include <iostream>
#include <string>

class InPatient;
std::ostream &operator<< (std::ostream &out, const InPatient &patient);

class InPatient : public Patient
{
    private:
    unsigned int daysIn = 0;
    unsigned int daysInToEnd = 0;
    bool canReceiveVisit;

    public:
    InPatient (std::string name, unsigned int daysInToEnd, bool canReceiveVisit);
    InPatient ();

    bool
    IsFinished ();

    unsigned int
    operator+= (unsigned int increment);

    bool
    CanReceiveVisit ();

    void
    SetCanReceiveVisit (bool visit);

    unsigned int
    operator-- ();

    /**
     * Sobrecarga do método de printar na tela
     */
    friend
    std::ostream &
    operator<< (std::ostream &out, InPatient const &patient)
    {
        out << patient.name << "\tvisits: ";
        if (patient.canReceiveVisit)
            std::cout << "yes\tdays in: ";
        else
            std::cout << "no\tdays in: ";
        std::cout << patient.daysIn << "\tdays left: " << patient.daysInToEnd;
        return out;
    }

    unsigned int
    GetDaysIn ();

    unsigned int
    GetDaysInToEnd ();

};


#endif