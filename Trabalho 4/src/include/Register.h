/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 04 DE OUTUBRO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef REGISTER_H
#define REGISTER_H

#include "Tree.h"
#include "Node.h"

template <typename contentType>
class Register
{
    private:
    Tree<contentType> *content;
    unsigned int size = 0;

    public:

    Register()
    {
        this->content = new Tree<contentType> ();
    }

    Register (Register<contentType> *copyRegister)
    {
        this->content = copyRegister->GetContent();
    }

    Tree<contentType> *
    GetContent ()
    {
        return this->content;
    }

    Node<contentType> *
    Insert (contentType newData) {
        return *(this->content) += newData;   
    }

    errorType
    Remove (contentType dataToRemove) {
        Tree<contentType> *newTree = new Tree<contentType> (), *oldTree;
        unsigned int newSize = 0;

        for (Node<contentType> node : Tree<contentType>::MakeVector(this->content))
        {            
            if (node.GetContent() != dataToRemove) {
                *newTree += node.GetContent();
                newSize ++;
            }
        }

        oldTree = this->content;
        this->content = newTree;
        delete oldTree;

        this->size = newSize;
        return errorType::ok;
    }

    Register<contentType> *
    operator+ (Register<contentType> cat)
    {
        Register<contentType> *result = new Register<contentType> ();

        for (Node<contentType> node : Tree<contentType>::MakeVector (this->content)) {
            result->Insert(node.GetContent());
        }
        for (Node<contentType> node : Tree<contentType>::MakeVector (cat.GetContent())) {
            result->Insert(node.GetContent());
        }

        return result;
    }

    Node<contentType> *
    operator() (std::string search)
    {
        return (*this->content)(search);
    }

    friend
    std::ostream &
    operator<< (std::ostream &out, Register<contentType> &reg)
    {
        out << *(reg.GetContent());
        return out;
    }

};

#endif