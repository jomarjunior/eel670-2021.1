/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 04 DE OUTUBRO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#include "InPatient.h"

InPatient::InPatient (std::string name, unsigned int daysInToEnd, bool canReceiveVisit)
{
    this->SetName(name);
    this->daysInToEnd = daysInToEnd;
    this->daysIn = 0;
    this->canReceiveVisit = canReceiveVisit;
}

InPatient::InPatient ()
{
    this->SetName("");
    this->daysInToEnd = 0;
    this->daysIn = 0;
    this->canReceiveVisit = true;
}

bool
InPatient::CanReceiveVisit ()
{
    return this->canReceiveVisit;
}

void
InPatient::SetCanReceiveVisit (bool visit)
{
    this->canReceiveVisit = visit;
}

bool
InPatient::IsFinished ()
{
    return this->daysInToEnd == 0;
}

unsigned int
InPatient::operator+= (unsigned int increment)
{
    this->daysInToEnd += increment;
    return this->daysInToEnd;
}

unsigned int
InPatient::operator-- ()
{
    this->daysIn++;
    this->daysInToEnd--;
    return this->daysIn;
}

unsigned int
InPatient::GetDaysIn ()
{
    return this->daysIn;
}

unsigned int
InPatient::GetDaysInToEnd ()
{
    return this->daysInToEnd;
}