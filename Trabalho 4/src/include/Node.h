/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 04 DE OUTUBRO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */
#ifndef NODE_H
#define NODE_H

#include "error.h"

#include <iostream>

using namespace error;

template <typename contentType> class Node;
template <typename contentType> std::ostream &operator<< (std::ostream &out, const Node<contentType> &node);

/**
 * Classe Nó para a Árvore binária
 */
template <typename contentType>
class Node
{
    private:
    contentType content;
    Node *left;
    Node *right;

    public:
    /**
     * Construtor da classe de nó da árvore binária
     * @param content conteúdo do nó.
     */
    Node (contentType &content)
    {
        this->content = content;
        this->left = NULL;
        this->right = NULL;
    }

    /**
     * Retorna a condição do nó ser ou não uma folha da árvore
     * @return True caso seja uma folha, False caso não seja.
     */
    bool
    isLeaf ()
    {
        return !this->left && !this->right;
    }

    /**
     * Operador que imprime na tela o conteúdo do nó.
     */
    friend
    std::ostream &
    operator<< (std::ostream &out, Node<contentType> &node)
    {
        out << node.GetContent();
        return out;
    }
    
    /**
     * Retorna o conteúdo do nó
     * @return Conteúdo do nó
     */
    contentType
    GetContent ()
    {
        return this->content;
    }

    /**
     * Define o conteúdo do nó
     * @param content conteúdo do nó.
     * @return Código de erro correspondente ou errorType::ok
     */
    errorType
    SetContent (contentType content)
    {
        this->content = content;
        return errorType::ok;
    }

    /**
     * Retorna o nó à esquerda
     * @return Ponteiro para o nó à esquerda deste
     */
    Node<contentType> *
    GetLeftNode ()
    {
        return this->left;
    }

    /**
     * Define o nó à esquerda
     * @param node nó a ser associado à esquerda deste
     * @return Código de erro correspondente ou errorType::ok
     */
    errorType
    SetLeftNode (Node *newLeftNode)
    {
        if (!newLeftNode) return errorType::nullParameter;

        this->left = newLeftNode;
        return errorType::ok;
    }

    /**
     * Retorna o nó à direita
     * @return Ponteiro para o nó à direita deste
     */
    Node<contentType> *
    GetRightNode ()
    {
        return this->right;
    }
        
    /**
     * Define o nó à direita
     * @param node nó a ser associado à direita deste
     * @return Código de erro correspondente ou errorType::ok
     */
    errorType
    SetRightNode (Node *newRightNode)
    {
        if (!newRightNode) return errorType::nullParameter;

        this->right = newRightNode;
        return errorType::ok;
    }

};

#endif