/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 04 DE OUTUBRO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef ITEM_ALREADY_INSIDE_THE_TREE
#define ITEM_ALREADY_INSIDE_THE_TREE

#include <iostream>
#include <exception>

namespace Exceptions {
    class ItemAlreadyInsideTheTree : public std::exception
    {
        public:

        const char *
        what() const throw ();
    };
}

#endif