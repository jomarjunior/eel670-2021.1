/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 04 DE OUTUBRO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#include "ItemAlreadyInsideTheTree.h"

#include <iostream>
#include <string>
#include <exception>

using namespace Exceptions;

const char *
ItemAlreadyInsideTheTree::what() const throw()
{
    return "O item a ser adicionado ja esta inserido na arvore de destino e nao pode ser repetido.";
}