/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 04 DE OUTUBRO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef ERROR
#define ERROR

#include <iostream>
#include <string>

/*
 * Namespace que guarda as constantes de erro do programa.
 */
namespace error
{
    typedef enum {
        ok,
        wrongOption,
        nullParameter,
        errorAmount
    } errorType;

    const std::string errorMessage[errorType::errorAmount] = 
    {
        "Ok!",
        "Opção errada!",
        "Parametro nulo fornecido a funcao!"
    };
}

#endif