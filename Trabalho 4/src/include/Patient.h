/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 04 DE OUTUBRO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef PATIENT_H
#define PATIENT_H

#include <iostream>
#include <string>

#include "error.h"

using namespace error;

class Patient;
std::ostream &operator<< (std::ostream &out, const Patient &patient);

class Patient
{
    protected:
    std::string name = "";

    public:

    /**
     * Construtor da classe paciente, espera uma string para o nome do paciente.
     * @param name std::string o nome do pacientw.
     */
    Patient(std::string name);
    Patient();

    /**
     * Método get do nome do paciente.
     * @return std::string o nome do paciente.
     */
    std::string
    GetName();

    /**
     * Método set do nome do paciente.
     * @param newName o novo nome do paciente.
     * @return errorType::ok ou código de error correspondente.
     */
    errorType
    SetName(std::string newName);

    /**
     * Sobrecarga do método de printar na tela
     */
    friend
    std::ostream &
    operator<< (std::ostream &out, Patient const &patient)
    {
        out << patient.name;
        return out;
    }

    bool
    operator== (Patient patient);

    bool
    operator!= (Patient patient);

    bool
    operator> (Patient patient);

    bool
    operator< (Patient patient);
};

#endif