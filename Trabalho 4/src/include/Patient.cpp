/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 04 DE OUTUBRO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#include "Patient.h"

#include <iostream>
#include <string>

#include "error.h"

using namespace error;

Patient::Patient (std::string name)
{
    this->name = name;
}

Patient::Patient ()
{
    this->name = "";
}


std::string
Patient::GetName ()
{
    return this->name;
}

errorType
Patient::SetName (std::string newName)
{
    this->name = newName;

    return errorType::ok;
}

bool
Patient::operator== (Patient patient)
{
    return this->name == patient.GetName();
}

bool
Patient::operator!= (Patient patient)
{
    return this->name != patient.GetName();
}

bool
Patient::operator> (Patient patient)
{
    return this->name > patient.GetName();
}

bool
Patient::operator< (Patient patient)
{
    return this->name < patient.GetName();
}