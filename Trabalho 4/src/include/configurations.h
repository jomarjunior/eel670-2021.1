/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 04 DE OUTUBRO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef CONFIGURATIONS   
#define CONFIGURATIONS

#include <iostream>
#include <string>
#include <getopt.h>

/*
 * Namespace que guarda constantes de configuração do programa.
 */
namespace configurations
{
    // Main.cpp
}

#endif