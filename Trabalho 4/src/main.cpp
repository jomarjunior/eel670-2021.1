/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 04 DE OUTUBRO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#include <iostream>
#include <string>
#include <random>

#include "include/error.h"
#include "include/configurations.h"
#include "include/Node.h"
#include "include/Tree.h"
#include "include/Register.h"
#include "include/Patient.h"
#include "include/InPatient.h"

#include "include/exceptions/ItemAlreadyInsideTheTree.h"

using namespace error;


int
main (int argc, char *argv[])
{
    Register<InPatient> *regist = new Register<InPatient> ();
    InPatient *newPatient;
    Node<InPatient> *nodePointer;
    std::string userInput;
    std::string name;
    unsigned int days;
    bool visit;

    try {
        while (userInput != "sair") {
            std::cout << "Digite o nome do paciente. Digite 'sair' para encerrar a insercao de pacientes : ";
            std::getline(std::cin, userInput);
            name = userInput;
            if (userInput == "sair") {
                break;
            }
            userInput = "";

            std::cout << "Digite a quantidade de dias de internacao do paciente. Digite 'sair' para encerrar a insercao de pacientes e descartar o atual : ";
            std::getline(std::cin, userInput);
            days = (unsigned int) std::stoul(userInput);
            if (userInput == "sair") {
                break;
            }
            userInput = "";

            while (userInput != "sim" && userInput != "nao") {
                std::cout << "O paciente podera receber visitas? (sim / nao) Digite 'sair' para encerrar a insercao de pacientes e descartar o atual : ";
                std::getline(std::cin, userInput);
                if (userInput == "sair") {
                    break;
                }
            }
            if (userInput == "sair") {
                break;
            }
            visit = userInput == "sim";
            userInput = "";

            newPatient = new InPatient(name, days, visit);
            if (!(regist->Insert(*newPatient))) throw Exceptions::ItemAlreadyInsideTheTree();
            std::cout << std::endl;
        }

        std::cout << "\nLista de Pacientes" << std::endl << *regist << std::endl;

        std::cout << "Busca de um paciente." << std::endl;

        userInput = "";
        while (userInput != "sair") {
            std::cout << "Digite o nome do/da paciente que deseja procurar. Digite 'sair' para encerrar a execucao." << std::endl;
            std::getline(std::cin, userInput);
            if ((nodePointer = (*regist)(userInput))) {
                std::cout << userInput << " ENCONTRADO!" << std::endl;
                std::cout << *nodePointer << std::endl;
            } else {
                std::cout << userInput << " nao encontrado!" << std::endl;
            }
        }
    } catch (Exceptions::ItemAlreadyInsideTheTree &e) {
        std::cout << "Erro!" << std::endl << e.what() << std::endl;
    }

    std::cout << "Fim da Execucao" << std::endl;

    return errorType::ok;
}