Trabalho 4 - EEL670 - 2021.1
===
**UNIVERSIDADE FEDERAL DO RIO DE JANEIRO**

*Escola Politécnica*

*Departamento de Engenharia Eletrônica e de Computação*

Aluno: Jomar Júnior de Souza Pereira

O programa consiste num sistema de cadastro de pacientes em um hospital. O usuário é capaz de adicionar novos pacientes que terão um nome e atributos variados, dependendo da implementação do polimorfismo da classe Patient definida em Patient.h. No exemplo do main.cpp, foi criada uma classe InPatient derivada da Patient que implementa pacientes internados no hospital, com dias de internação, possibilidade de visitas e dias restantes.

Estrutura
---
Foram implementadas 3 classes templates, 2 classes e 1 classe exception que atuam de forma distinta. São elas:

* **Patient**

    Classe base para os pacientes do hospital, implementa sobrecarga de operadores de comparação, print e possui um nome em string.

* **InPatient**

    Classe que herda atributos e metodos da classe Patient. Implementa pacientes internados com data de internação, possibilidade de visitas e dias restantes de internação.

* **Node**

    Classe template que implementa um nó de uma árvore binária, com adjacência à esquerda e à direita.

* **Tree**

    Classe template que implementa uma árvore binária, possui uma raiz de Node e métodos característicos de uma árvore binária, como inserção, busca, etc.

* **Register**

    Classe template que implementa o cadastro dos pacientes, possui uma árvore binária onde estarão registrados os dados.

* **ItemAlreadyInsideTheTree**

    Classe exception que cuida de informar o usuário de um erro relacionado à inserção de novos itens na árvore. Informa que não é possível inserir um paciente de mesmo nome na árvore. É lançado na main quando a função de inserção retorna NULL.

Por fim, existem mais dois arquivos de cabeçalho que implementam namespaces:

* **configurations**

    Namespace que contém todas as constantes de configuração do programa, como opções de linha de comando, quantidade de argumentos esperada, etc...

* **error**

    Namespace que contém as constantes de erro que o programa retorna em diversos casos, além das mensagens relacionadas.

Todos esses arquivos devem estar dentro do diretório include/ que por sua vez deve estar no diretório src/, junto do arquivo **main.cpp** e do **makefile**.

Instruções de Compilação
---
A compilação do programa é de certa forma bem simples. São os arquivos necessários:

- main.cpp
- include/
    - exceptions/
        - ItemAlreadyInsideTheTree.cpp
        - ItemAlreadyInsideTheTree.h
    - configurations.h
    - error.h
    - InPatient.cpp
    - InPatient.h
    - Node.h
    - Patient.cpp
    - Patient.h
    - Register.h
    - Tree.h

Com esses arquivos, o compilador pode ser chamado da seguinte maneira após a criação dos objetos de linkedição de cada um dos **.cpp**:
```ubuntu
g++ -Wall -std=c++17 -o main.exe main.o ItemAlreadyInsideTheTree.o InPatient.o Patient.o
```

Instruções de Utilização
---

## Execução
A execução do programa é direta e sem opções de linha de comando. Exemplo de execução:
```ubuntu
./main.exe

O programa irá requisitar entradas de texto do usuário de forma dinâmica, fornecendo instruções na hora em que forem necessárias.