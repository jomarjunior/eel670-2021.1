/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 13 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#include <iostream>
#include <string>
#include <vector>

#include <getopt.h>
#include <stdlib.h>

#include "./include/Estadual.hpp"
#include "./include/Classificacao.hpp"
#include "./include/Nacional.hpp"
#include "./include/configuracoes.hpp"
#include "./include/erro.hpp"
#include "./include/Mensageiro.hpp"
#include "./include/Populador.hpp"

int
main (int argc, char *argv[])
{
    // Declarações iniciais
    Mensageiro *mensageiro = new Mensageiro;
    Nacional *Brasil = new Nacional ("BRASIL");
    Populador *populador = new Populador (Brasil);

    int indiceOpcoes = 0;
    char opcao = '\0';

    // Popular o país usando os dados guardados em um arquivo .csv
    populador->PopularPorCSV("../dados/dados.csv");


    // Checar a quantidade de argumentos
    if (argc < configuracoes::numeroDeArgumentos){
        mensageiro->ExibirMensagemAjuda();
        exit(erro::comandoErrado);
    }

    // Ler os argumentos de linha de comando e obter as long_options ou short_options usadas.
    while ((opcao = getopt_long(argc, argv, configuracoes::opcoesPeq.c_str(), configuracoes::opcoes, &indiceOpcoes)) > -1) {
        switch (opcao) {
            // Evolução dos casos de óbito pela média móvel
            case 'e':
                mensageiro->ExibirEvolucao(Brasil, strtoul(optarg, NULL, 10));
                exit(erro::ok);
            break;

            // Classificação da evolução da média movel dos estados.
            case 'c':
                mensageiro->ExibirClassificacaoEstadual(Brasil, strtoul(optarg, NULL, 10));
                exit(erro::ok);
            break;

            // Classificação da evolução da média movel do país.
            case 'C':
                mensageiro->ExibirClassificacaoNacional(Brasil, strtoul(optarg, NULL, 10));
                exit(erro::ok);
            break;

            // Estados extremos do país para mais e menos.
            case 'x':
                mensageiro->ExibirExtremos (Brasil, strtoul(optarg, NULL, 10));
                exit(erro::ok);
            break;

            // Acúmulo de óbitos diários nos estados e no país.
            case 'a':
                mensageiro->ExibirAcumuloObitos (Brasil);
                exit(erro::ok);
            break;

            // Página de ajuda.
            case 'h':
                mensageiro->ExibirMensagemAjuda();
                exit(erro::ok);
            break;

            // Opção errada.
            default:
                mensageiro->ExibirMensagemAjuda();
                exit(erro::comandoErrado);
            break;
        }
    }

    return erro::ok;
}