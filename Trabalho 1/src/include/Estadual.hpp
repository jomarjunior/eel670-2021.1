/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 13 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef ESTADUAL_HPP
#define ESTADUAL_HPP

#include <iostream>
#include <vector>
#include "Classificacao.hpp"

class Estadual {
    private:
    Classificacao *classificacao;
    std::string nome;
    int nRegistros;
    std::vector<int> historico;
    long double mediaMovel;
    int acumuloObitos;
    double porcentagemClassificacao;

    public:
    Estadual(std::string, std::vector<int>);

    long double
    CalcularMediaMovel (int, int = 0);

    void
    CalcularClassificacao (int);

    long double
    GetMediaMovel ();

    std::string
    GetNome ();

    Classificacao *
    GetClassificacao ();

    int
    GetAcumuloObitos ();

    std::vector<int>
    GetHistorico ();

    double
    GetPorcentagemClassificacao ();
};

#endif