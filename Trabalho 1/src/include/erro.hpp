/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 13 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef ERRO
#define ERRO

/*
 * Namespace que guarda as constantes de erro do programa.
 */
namespace erro
{
    const int ok = 0;
    const int comandoErrado = 1;
}

#endif