/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 13 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#include "Nacional.hpp"

#include <iostream>
#include <vector>
#include <string>

#include "Estadual.hpp"

/*
 * Construtor da classe.
 * _nome (std::string) - Nome do país a ser criado.
 */
Nacional::Nacional (std::string _nome)
{
    this->nome = _nome;
}

/*
 * Adiciona um estado ao final da lista do país.
 * novoEstado (Estadual *) - Novo estado a ser adicionado ao final da lista.
 */
void
Nacional::AdicionarEstado (Estadual *novoEstado)
{
    this->estados.push_back(*novoEstado);
}

/*
 * Calcula a media móvel dos estados do país e retorna a soma (long double), baseado no número de termos e no offset definidos.
 * nTermos (int) - Número de dias a ser computado na media móvel.
 * offset (int) - Número de dias antes do dia atual onde será iniciada a contagem.
 */
void
Nacional::CalcularMediaMovel (int nTermos, int offset)
{
    this->mediaMovel = 0;
    for (Estadual estado : this->estados) {
        estado.CalcularMediaMovel(nTermos, offset);
        this->mediaMovel += estado.GetMediaMovel();
    }
}

/*
 * Calcula a classificação da evolução da media móvel do país.
 * nTermos (int) - Quantidade de dias a serem computados no cálculo.
 */
void
Nacional::CalcularClassificacao (int nTermos)
{
    this->CalcularMediaMovel(nTermos, 1);
    double diaAnterior = this->GetMediaMovel();
    this->CalcularMediaMovel(nTermos);
    double diaAtual = this->GetMediaMovel();

    if (!this->classificacao) this->classificacao = new Classificacao ((double)(diaAtual - diaAnterior)* 100/(diaAnterior));
    else this->classificacao->Atualizar ((double)(diaAtual - diaAnterior)* 100/(diaAnterior));
}

/*
 * Retorna a lista de estados do pais. (std::vector<Estadual>)
 */
std::vector<Estadual>
Nacional::GetEstados ()
{
    return this->estados;
}

/*
 * Calcula e retorna std::vector<Estadual> a lista de estados com a maior media móvel num determinado período.
 * nTermos (int) - Quantidade de dias a serem computados.
 */
std::vector<Estadual>
Nacional::GetMaiorMedia (int nTermos)
{
    std::vector<Estadual> maioresMedias;

    for (Estadual estado : this->estados) {
        if (maioresMedias.size() == 0) {
            maioresMedias.push_back(estado);
            continue;
        }
        if (maioresMedias[0].CalcularMediaMovel(nTermos) < estado.CalcularMediaMovel(nTermos)) {
            maioresMedias.clear();
            maioresMedias.push_back(estado);
        } else if (maioresMedias[0].CalcularMediaMovel(nTermos) == estado.CalcularMediaMovel(nTermos)) {
            maioresMedias.push_back(estado);
        }
    }

    return maioresMedias;
}

/*
 * Calcula e retorna std::vector<Estadual> a lista de estados com a menor media móvel num determinado período.
 * nTermos (int) - Quantidade de dias a serem computados.
 */
std::vector<Estadual>
Nacional::GetMenorMedia (int nTermos)
{
    std::vector<Estadual> menoresMedias;

    for (Estadual estado : this->estados) {
        if (menoresMedias.size() == 0){
            menoresMedias.push_back(estado);
            continue;
        }
        if (menoresMedias[0].CalcularMediaMovel(nTermos) > estado.CalcularMediaMovel(nTermos)) {
            menoresMedias.clear();
            menoresMedias.push_back(estado);
        } else if (menoresMedias[0].CalcularMediaMovel(nTermos) == estado.CalcularMediaMovel(nTermos)) {
            menoresMedias.push_back(estado);
        }
    }

    return menoresMedias;
}

/*
 * Retorna o nome do pais. (std::string)
 */
std::string
Nacional::GetNome ()
{
    return this->nome;
}

/*
 * Retorna a media movel do pais. (double)
 */
double
Nacional::GetMediaMovel ()
{
    return this->mediaMovel;
}

/*
 * Retorna a classificação do pais. (Classificacao *)
 */
Classificacao *
Nacional::GetClassificacao ()
{
    return this->classificacao;
}

/*
 * Calcula o acúmulo de obitos dos estados deste país e retorna a soma. (int)
 */
int
Nacional::GetAcumuloObitos ()
{
    this->acumuloObitos = 0;
    for (Estadual estado : this->estados) {
        this->acumuloObitos += estado.GetAcumuloObitos();
    }
    return this->acumuloObitos;
}