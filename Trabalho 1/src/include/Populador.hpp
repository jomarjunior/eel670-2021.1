/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 13 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef POPULADOR
#define POPULADOR

#include "Nacional.hpp"

class Populador
{
    private:
    Nacional *pais;

    public:
    Populador (Nacional *);

    void
    PopularPorCSV (std::string);
};

#endif