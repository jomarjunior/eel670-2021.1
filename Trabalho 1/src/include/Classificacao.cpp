/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 13 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#include "Classificacao.hpp"

#include <iostream>
#include "configuracoes.hpp"

/*
 * Construtor da classe.
 * _porcentagem (entrada/double) - taxa percentual de evolução da média móvel.
 */
Classificacao::Classificacao (double _porcentagem)
{
    this->porcentagem = _porcentagem;
    
    if (_porcentagem >= configuracoes::limiteSuperior)          this->id = 2;
    else if (_porcentagem <= configuracoes::limiteInferior)     this->id = 1;
    else                                                        this->id = 0;

    this->texto = configuracoes::textoClassificacao[this->id];
}

/*
 * Atualiza o valor da classificação baseado numa nova taxa de evolução.
 * porcentagem (entrada/double) - nova taxa percentual de evolução da média móvel.
 */
void
Classificacao::Atualizar (double porcentagem)
{
    this->porcentagem = porcentagem;

    if (porcentagem >= configuracoes::limiteSuperior)          this->id = 2;
    else if (porcentagem <= configuracoes::limiteInferior)     this->id = 1;
    else                                                       this->id = 0;

    this->texto = configuracoes::textoClassificacao[this->id];
}

/*
 * Retorna o Id (int) da classificação.
 */
int
Classificacao::GetId()
{
    return this->id;
}

/*
 * Retorna o Texto (std::string) da classificação.
 */
std::string
Classificacao::GetTexto()
{
    return this->texto;
}

/*
 * Retorna a porcentagem (double) da classificação.
 */
double
Classificacao::GetPorcentagem()
{
    return this->porcentagem;
}