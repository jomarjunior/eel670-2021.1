/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 13 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef CLASSIFICACAO
#define CLASSIFICACAO

#include <iostream>
#include <string>

class Classificacao {

    private:
    int id;
    std::string texto;
    double porcentagem;

    public:
    Classificacao (double);

    void
    Atualizar (double);

    int
    GetId();

    std::string
    GetTexto();

    double
    GetPorcentagem ();

};

#endif