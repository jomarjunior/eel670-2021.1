/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 13 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#include "Populador.hpp"

#include <iostream>
#include <string>

#include "Nacional.hpp"
#include "Estadual.hpp"

#include "rapidcsv.h"

/*
 * Construtor da classe. Recebe o país que deverá ser populado.
 * _pais (Nacional *) - País a ser populado.
 */
Populador::Populador (Nacional *_pais)
{
    this->pais = _pais;
}

/*
 * Popula o país utilizando um arquivo .csv.
 * Este método lança mão da biblioteca rapidcsv.h.
 * nomeCSV (std::string) - localização do arquivo no sistema de arquivos do OS.
 */
void
Populador::PopularPorCSV (std::string nomeCSV)
{
    // Função para abrir o arquivo
    rapidcsv::Document doc(nomeCSV, rapidcsv::LabelParams(-1, 0));

    // Obter os nomes das linhas.
    std::vector<std::string> linhas = doc.GetRowNames();

    // Iterar nas linhas
    for (std::string linha : linhas) {
        // Obter os valores das colunas desta linha.
        std::vector<int> valores = doc.GetRow<int>(linha);
        // Criar um estado novo com o nome da linha e os valores.
        Estadual *novoEstado = new Estadual (linha, valores);
        // Adicionar o estado ao país.
        this->pais->AdicionarEstado(novoEstado);
    }

}