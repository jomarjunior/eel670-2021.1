/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 13 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef MENSAGEIRO
#define MENSAGEIRO

#include <iostream>
#include <string>

#include "Estadual.hpp"
#include "Nacional.hpp"

class Mensageiro
{
    private:

    public:

    void
    ExibirMensagemAjuda ();

    void
    ExibirEvolucao (Nacional *, unsigned long);

    void
    ExibirClassificacaoEstadual (Nacional *, unsigned long);

    void
    ExibirClassificacaoNacional (Nacional *, unsigned long);

    void
    ExibirExtremos (Nacional *, unsigned long);

    void
    ExibirAcumuloObitos (Nacional *);
};

#endif