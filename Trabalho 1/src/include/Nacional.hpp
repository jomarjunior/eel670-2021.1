/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 13 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef NACIONAL
#define NACIONAL

#include "Estadual.hpp"
#include "Classificacao.hpp"
#include <iostream>
#include <vector>
#include <string>

class Nacional
{
    private:
    Classificacao *classificacao;
    std::vector<Estadual> estados;
    std::string nome;
    double mediaMovel;
    int acumuloObitos;

    public:
    Nacional (std::string);

    void
    AdicionarEstado (Estadual *);

    void
    CalcularMediaMovel (int, int = 0);

    void
    CalcularClassificacao (int);

    std::string
    GetNome ();

    double
    GetMediaMovel ();

    Classificacao *
    GetClassificacao ();

    std::vector<Estadual>
    GetEstados ();

    std::vector<Estadual>
    GetMaiorMedia (int);

    std::vector<Estadual>
    GetMenorMedia (int);

    int
    GetAcumuloObitos ();
};

#endif