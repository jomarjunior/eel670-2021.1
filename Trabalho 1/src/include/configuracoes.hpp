/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 13 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef CONFIGURACOES   
#define CONFIGURACOES

#include <iostream>
#include <string>
#include <getopt.h>

/*
 * Namespace que guarda constantes de configuração do programa.
 */
namespace configuracoes
{
    // Main.cpp
    const struct option opcoes[] = {
        {"evolucao", required_argument, 0, 'e'},
        {"classificacao.estados", required_argument, 0, 'c'},
        {"classificacao.nacional", required_argument, 0, 'C'},
        {"extremos", required_argument, 0, 'x'},
        {"acumulado", no_argument, 0, 'a'},
        {"help", no_argument, 0, 'h'},
        {0, 0, 0, 0}
    };
    const std::string opcoesPeq = "e:c:C:x:ah";
    const int numeroDeArgumentos = 2;

    // Classificacao.cpp
    const int limiteSuperior = 10;
    const int limiteInferior = -10;
    const std::string textoClassificacao[3] = {"Estabilidade", "Baixa", "Alta"};
    const int estabilidade = 0;
    const int baixa = 1;
    const int alta = 2;
}

#endif