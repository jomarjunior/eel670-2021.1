/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 13 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#include "Mensageiro.hpp"

#include <iostream>
#include <string>
#include <vector>

#include "configuracoes.hpp"

/*
 * Exibe na tela uma mensagem contendo todos os comandos aceitos pelo programa.
 */
void
Mensageiro::ExibirMensagemAjuda ()
{
    std::cout << "Comandos aceitos: \n\n";
    std::cout << "\t--evolucao | -e\n\t(\"--evolucao <numero de termos da media movel>\")\tExibir a evolucao do numero de obitos no Brasil e nos estados individualmente usando a media movel.\n\n";
    std::cout << "\t--classificacao.estados | -c\n\t(\"--classificacao.estados <numero de termos da media movel>\")\tExibir de forma agrupada os estados em alta, estabilidade e baixa segundo a razao entre a media movel do dia atual e do dia anterior.\n\n";
    std::cout << "\t--classificacao.nacional | -C\n\t(\"--classificacao.nacional <numero de termos da media movel>\")\tExibir a classificacao do Brasil segundo a razao entre a media movel do dia atual e do dia anterior.\n\n";
    std::cout << "\t--extremos | -x\n\t(\"--extremos <numero de termos da media movel>\")\tExibir o estado com maior alta e maior baixa segundo a media movel do dia atual.\n\n";
    std::cout << "\t--acumulado | -a\n\t(\"--acumulado\")\tExibir os dados acumulados de numero de obitos no Brasil e nos estados individualmente.\n\n";
    std::cout << "\t--help | -h\n\t(\"--help\")\tExibir esta lista de comandos.\n";
}

/*
 * Exibe na tela uma mensagem contendo a evolução da media móvel dos estados e do país fornecido.
 * pais (Nacional *) - País para o qual as informações serão mostradas.
 * nTermos (unsigned long) - Quantidade de dias que serão computados.
 */
void
Mensageiro::ExibirEvolucao (Nacional *pais, unsigned long nTermos)
{
    // Atualiar a media móvel do país.
    pais->CalcularMediaMovel(nTermos);
                    
    std::cout << "Media Movel" << "\t" << "Nome" << std::endl;
    std::cout << "-------------------------------" << std::endl;

    // Iterar dentro dos estados
    for (Estadual estado : pais->GetEstados()) {
        // Atualizar a media móvel de cada um dos estados.
        estado.CalcularMediaMovel(nTermos);
        std::cout << estado.GetMediaMovel() << "\t\t" << estado.GetNome() << std::endl;
    }
    std::cout << pais->GetMediaMovel() << "\t\t" << pais->GetNome() << std::endl;
}

/*
 * Exibe na tela uma mensagem contendo os estados do país agrupados com base nas suas respectivas classificações.
 * pais (Nacional *) - País para o qual as informações serão mostradas.
 * nTermos (unsigned long) - Quantidade de dias que serão computados.
 */
void
Mensageiro::ExibirClassificacaoEstadual (Nacional *pais, unsigned long nTermos)
{
    // Iterar os estados e atualizar suas classificações
    for (Estadual estado : pais->GetEstados())    estado.CalcularClassificacao (nTermos);

    // EM ALTA -------------------
    std::cout << "EM ALTA: \n";
    for (Estadual estado : pais->GetEstados()) {
        if (estado.GetClassificacao()->GetId() == configuracoes::alta)
            std::cout <<
            "\t\t" <<
            estado.GetNome() <<
            " | Porcentagem : " <<
            estado.GetClassificacao()->GetPorcentagem() <<
            " %" <<
            std::endl;
    }
    std::cout << std::endl;

    // EM ESTABILIDADE -------------------
    std::cout << "EM ESTABILIDADE: \n";
    for (Estadual estado : pais->GetEstados()) {
        if (estado.GetClassificacao()->GetId() == configuracoes::estabilidade)
            std::cout <<
            "\t\t" <<
            estado.GetNome() <<
            " | Porcentagem : " <<
            estado.GetClassificacao()->GetPorcentagem() <<
            " %" <<
            std::endl;
    }
    std::cout << std::endl;

    // EM BAIXA -------------------
    std::cout << "EM BAIXA: \n";
    for (Estadual estado : pais->GetEstados()) {
        if (estado.GetClassificacao()->GetId() == configuracoes::baixa)
            std::cout <<
            "\t\t" <<
            estado.GetNome() <<
            " | Porcentagem : " <<
            estado.GetClassificacao()->GetPorcentagem() <<
            " %" <<
            std::endl;
    }
    std::cout << std::endl;
}

/*
 * Exibe na tela uma mensagem contendo a classificação do país.
 * pais (Nacional *) - País para o qual as informações serão mostradas.
 * nTermos (unsigned long) - Quantidade de dias que serão computados.
 */
void
Mensageiro::ExibirClassificacaoNacional (Nacional *pais, unsigned long nTermos)
{
    // Atualizar a classificação do país
    pais->CalcularClassificacao(nTermos);

    // Print longo
    std::cout << pais->GetNome() << " :: " << pais->GetClassificacao()->GetTexto() << " | Porcentagem: " << pais->GetClassificacao()->GetPorcentagem() << " %" << std::endl;
}

/*
 * Exibe na tela uma mensagem contendo os estados com a maior e a menor média móvel.
 * pais (Nacional *) - País para o qual as informações serão mostradas.
 * nTermos (unsigned long) - Quantidade de dias que serão computados.
 */
void
Mensageiro::ExibirExtremos (Nacional *pais, unsigned long nTermos)
{
    // Maiores medias
    std::vector<Estadual> estadosMaioresMedias = pais->GetMaiorMedia(nTermos);

    std::cout << ((estadosMaioresMedias.size() > 1) ? "Estados com Maior Media: " : "Estado com Maior Media: ") << estadosMaioresMedias[0].CalcularMediaMovel(nTermos) << std::endl;
    
    for (Estadual estado : estadosMaioresMedias) {
        std::cout << "\t\t" << estado.GetNome() << "\t";
    }
    
    std::cout << std::endl;

    // Menores medias
    std::vector<Estadual> estadosMenoresMedias = pais->GetMenorMedia(nTermos);
    
    std::cout << ((estadosMenoresMedias.size() > 1) ? "Estados com Menores Medias: " : "Estado com Menor Media: ") << estadosMenoresMedias[0].CalcularMediaMovel(nTermos) << std::endl;
    
    for (Estadual estado : estadosMenoresMedias) {
        std::cout << "\t\t" << estado.GetNome() << "\t";
    }
    
    std::cout << std::endl;
}

/*
 * Exibe na tela uma mensagem contendo o acúmulo de óbitos dos estados e do país.
 * pais (Nacional *) - País para o qual as informações serão mostradas.
 */
void
Mensageiro::ExibirAcumuloObitos (Nacional *pais)
{
    std::cout << "Acumulo de Obitos por Localidade" << std::endl;
    for (Estadual estado : pais->GetEstados()) {
        std::cout << estado.GetAcumuloObitos() << "\t" << estado.GetNome()  << std::endl;
    }
    std::cout << pais->GetAcumuloObitos() << "\t" << pais->GetNome() << std::endl;
}