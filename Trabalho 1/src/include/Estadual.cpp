/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 13 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#include "Estadual.hpp"
#include <iostream>
#include <vector>

/*
 * Construtor da classe.
 * _nome (std::string) - Nome do estado a ser criado.
 * registros (std::vector<int>) - Vetor de inteiros dos dados do número de óbitos deste estado ordenado diariamente.
 */
Estadual::Estadual(std::string _nome, std::vector<int> registros)
{
    this->nome = _nome;
    this->nRegistros = registros.size();
    this->historico = registros;

    this->classificacao = new Classificacao (0);
}

/*
 * Calcula a media móvel do estado e a retorna (long double), baseado no número de termos e no offset definidos.
 * nTermos (int) - Número de dias a ser computado na media móvel.
 * offset (int) - Número de dias antes do dia atual onde será iniciada a contagem.
 */
long double
Estadual::CalcularMediaMovel (int nTermos, int offset)
{
    this->mediaMovel = 0;
    int inicio = this->nRegistros - offset;
    for (int indice = inicio - 1; indice >= inicio - nTermos; indice --){
        this->mediaMovel += this->historico[indice];
    }
    this->mediaMovel /= nTermos;

    return this->GetMediaMovel();
}

/*
 * Calcula a classificação da evolução da media móvel do estado.
 * nTermos (int) - Quantidade de dias a serem computados no cálculo.
 */
void
Estadual::CalcularClassificacao (int nTermos)
{
    this->CalcularMediaMovel(nTermos, 1);
    int diaAnterior = this->GetMediaMovel();
    this->CalcularMediaMovel(nTermos);
    int diaAtual = this->GetMediaMovel();

    this->porcentagemClassificacao = (double)(diaAtual - diaAnterior)* 100/(diaAnterior);
    if (!this->classificacao) this->classificacao = new Classificacao (this->porcentagemClassificacao);
    else this->classificacao->Atualizar (this->porcentagemClassificacao);
}

/*
 * Retorna a media móvel (long double).
 */
long double
Estadual::GetMediaMovel ()
{
    return this->mediaMovel;
}

/*
 * Retorna o nome (std::string).
 */
std::string
Estadual::GetNome ()
{
    return this->nome;
}

/*
 * Retorna a classificação (Classificacao *).
 */
Classificacao *
Estadual::GetClassificacao ()
{
    return this->classificacao;
}

/*
 * Calcula e retorna o acúmulo de óbitos (int).
 */
int
Estadual::GetAcumuloObitos ()
{
    this->acumuloObitos = 0;
    for (int obitoDiario : this->historico) {
        this->acumuloObitos += obitoDiario;
    }
    return this->acumuloObitos;
}

/*
 * Retorna o histórico de óbitos do estado. (std::vector<int>).
 */
std::vector<int>
Estadual::GetHistorico ()
{
    return this->historico;
}

/*
 * Retorna a porcentagem da classificação do estado (double).
 */
double
Estadual::GetPorcentagemClassificacao ()
{
    return this->porcentagemClassificacao;
}