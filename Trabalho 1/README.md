Trabalho 1 - EEL670 - 2021.1
===
**UNIVERSIDADE FEDERAL DO RIO DE JANEIRO**

*Escola Politécnica*

*Departamento de Engenharia Eletrônica e de Computação*

Aluno: Jomar Júnior de Souza Pereira

O programa consiste de um pequeno sistema de manipulação de dados referentes aos obitos por COVID-19 em um determinado país. É possível criar países e populá-los com estados que por sua vez serão preenchidos por dados relacionados à pandemia.

Estrutura
---
Foram implementados 5 classes que atuam de forma distinta. São elas:

* **Classificacao** *(double Porcentagem)*

    Classe que controla a classificação de um determinado estado ou país, baseado na porcentagem de evolução da media móvel deste.

* **Estadual** *(std::string Nome, std::vector\<int> Dados Diarios)*]

    Classe que controla os estados de um determinado país, possuem sempre um objeto *Classificacao* associado. Guardam um *vector\<int>* ordenado dos dados de óbitos diários.

* **Mensageiro** *( )*

    Classe que controla as mensagens com o usuário, gerando strings que serão enviadas para o *std::cout*.

* **Nacional** *(std::string Nome)*

    Classe que controla os países. Possuem sempre zero ou mais *Estadual* associados e um *Classificacao*. Consegue controlar os cálculos dos *Estadual* associados e gerar seus próprios calculos baseados neles.

* **Populador** *(Nacional *Pais)*

    Classe responsável por popular um país com os dados de seus estados. Ela cria os objetos *Estadual* necessários baseados num arquivo **.csv** e os adiciona num *vector\<Estadual>* que é passado, por fim, ao país associado ao objeto na construção.

Além disso, o código ainda lança mão de uma classe pronta chamada [*rapidcsv.h*](https://github.com/d99kris/rapidcsv) -*Copyright (C) 2017-2021 Kristofer Berggren*- que é responsável por lidar com a burocracia de importar os dados de um arquivo **.csv**.

Por fim, existem mais dois arquivos de cabeçalho que implementam namespaces:

* **configuracoes**

    Namespace que contém todas as constantes de configuração do programa, como opções de linha de comando, quantidade de argumentos esperada, limites de classificação, etc...

* **erro**

    Namespace que contém as constantes de erro que o programa retorna em diversos casos.

Todos esses arquivos devem estar dentro do diretório include/ que por sua vez deve estar no diretório src/, junto do arquivo **main.cpp** e do **makefile**.

Instruções de Compilação
---
A compilação do programa é de certa forma bem simples. São os arquivos necessários:

- main.cpp
- include/
    - Classificacao.hpp
    - Classificacao.cpp
    - configuracoes.hpp
    - erro.hpp
    - Estadual.hpp
    - Estadual.cpp
    - Mensageiro.hpp
    - Mensageiro.cpp
    - Nacional.hpp
    - Nacional.cpp
    - Populador.hpp
    - Populador.cpp
    - rapidcsv.h

Com esses arquivos, o compilador pode ser chamado da seguinte maneira após a criação dos objetos de linkedição de cada um dos **.cpp**:
```ubuntu
g++ -Wall -std=c++17 -o main.exe main.o include/Classificacao.o include/Estadual.o include/Mensageiro.o include/Nacional.o include/Populador.o
```

Instruções de Utilização
---
## Arquivo dados.csv
O programa faz a leitura de um arquivo **.csv** localizado em *../dados/dados.csv* relativo ao arquivo **main.exe**, portanto, é necessário que o mesmo exista para que haja a população dos dados do país.
O arquivo **dados.csv** deve seguir o exemplo:
```.csv
Rio de Janeiro, 10, 12, 23, 54, 90, 74, 13
Minas Gerais, 7, 1, 3, 1, 90, 15, 1
Espirito Santo, 34, 45, 56, 67, 78, 89, 90
Bahia, 0, 1, 0, 3, 4, 2, 6
Tocantins, 0, 1, 0, 3, 4, 2, 6
Sergipe, 34, 45, 56, 67, 78, 89, 90
Nome, <int>, <int>, <int>, <int>, <int>, <int>, <int>
```
Para remover um estado é necessário apenas remover a linha em que o mesmo aparece.

Para adicionar um estado é necessário apenas adicionar uma nova linha em qualquer ordem com o seu nome seguida dos dados referentes a ele.

## Execução
A execução do programa espera uma opção de linha de comando para indicar qual a intenção do usuário. Exemplo de execução:
```ubuntu
./main.exe --evolucao 3
```
Lista de comandos:
```
--evolucao | -e
("--evolucao <numero de termos da media movel>")
Exibir a evolucao do numero de obitos no Brasil e nos estados individualmente usando a media movel.

--classificacao.estados | -c
("--classificacao.estados <numero de termos da media movel>")
Exibir de forma agrupada os estados em alta, estabilidade e baixa segundo a razao entre a media movel do dia atual e do dia anterior.

--classificacao.nacional | -C
("--classificacao.nacional <numero de termos da media movel>")
Exibir a classificacao do Brasil segundo a razao entre a media movel do dia atual e do dia anterior.

--extremos | -x
("--extremos <numero de termos da media movel>")
Exibir o estado com maior alta e maior baixa segundo a media movel do dia atual.

--acumulado | -a
("--acumulado")
Exibir os dados acumulados de numero de obitos no Brasil e nos estados individualmente.

--help | -h
("--help")
Exibir esta lista de comandos.
```