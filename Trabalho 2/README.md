Trabalho 2 - EEL670 - 2021.1
===
**UNIVERSIDADE FEDERAL DO RIO DE JANEIRO**

*Escola Politécnica*

*Departamento de Engenharia Eletrônica e de Computação*

Aluno: Jomar Júnior de Souza Pereira

O programa consiste num sistema de analise de grafos simétricos. Dado um grafo simétrico, é possível obter o número de vértices, enlaces, o menor caminho entre dois vértices, o diâmetro, o vértice de maior centralidade de grau e o vértice de maior centralidade de intermediação.

Estrutura
---
Foram implementadas 9 classes que atuam de forma distinta. São elas:

* **Dijkstra**

    Classe que controla a execução do algoritmo de dijkstra para encontrar o menor caminho entre dois vértices do grafo. Também cuida de outras informações relacionadas, como o diâmetro e o vértice de maior centralidade de intermediação.

* **Edge**

    Classe que controla as informações das arestas, possui dois vértices e um peso associado. Tem a capacidade de criar novos vértices a partir de rótulos fornecidos na sua construção.

* **FileHandler**

    Classe que cuida da abertura e do fechamento do arquivo contendo as informações de construção do grafo.

* **Graph**

    Classe que cuida das informações referentees ao grafo. Não tem a capacidade de criar novas arestas, apenas associar as fornecidas.

* **Messenger**

    Classe principal responsável por imprimir na tela as diversas informações do programa.

* **PairMaker**

    Classe auxiliar que possui um método estático responsável por retornar todas as combinações de pares de uma determinada lista de vértice.

* **Path**

    Classe responsável por guardar as informações sobre caminhos dentro do grafo.

* **Populator**

    Classe responsável por popular o grafo com os dados do arquivo lido anteriormente. Ela cria as arestas e alimenta o grafo.

* **Vertex**

    Classe responsável por guardar as informações de um único vértice.

Por fim, existem mais dois arquivos de cabeçalho que implementam namespaces:

* **configurations**

    Namespace que contém todas as constantes de configuração do programa, como opções de linha de comando, quantidade de argumentos esperada, etc...

* **error**

    Namespace que contém as constantes de erro que o programa retorna em diversos casos, além das mensagens relacionadas.

Todos esses arquivos devem estar dentro do diretório include/ que por sua vez deve estar no diretório src/, junto do arquivo **main.cpp** e do **makefile**.

Instruções de Compilação
---
A compilação do programa é de certa forma bem simples. São os arquivos necessários:

- main.cpp
- include/
    - configurations.h
    - Dijkstra.h
    - Dijkstra.cpp
    - Edge.h
    - Edge.cpp
    - error.h
    - FileHandler.cpp
    - FileHandler.h
    - Graph.cpp
    - Graph.h
    - Messenger.cpp
    - Messenger.h
    - PairMaker.cpp
    - PairMaker.h
    - Path.cpp
    - Path.h
    - Populator.cpp
    - Populator.h
    - Vertex.cpp
    - Vertex.h

Com esses arquivos, o compilador pode ser chamado da seguinte maneira após a criação dos objetos de linkedição de cada um dos **.cpp**:
```ubuntu
g++ -Wall -std=c++17 -o main.exe main.o Messenger.o FileHandler.o Vertex.o Edge.o Path.o Graph.o Populator.o Dijkstra.o PairMaker.o
```

Instruções de Utilização
---
## Arquivo de dados
O programa faz a leitura de um arquivo **.csv** que deve ser fornecido como um argumento de linha de comando, portanto, é necessário que o mesmo exista para que haja a população dos dados do grafo.
O arquivo deve seguir o exemplo:
```.csv
Pedro Paulo 4
Pedro Jessica 3
Pedro Leopoldo 1
Paulo Jessica 7
Paulo Leopoldo 3
Paulo Jasmine 2
Paulo Roberta 5
Jessica Roberta 8
Jessica Leopoldo 1
Leopoldo Roberta 3
Leopoldo Denise 1
Jasmine Roberta 2
Jasmine Denise 1
Roberta Denise 1
Denise Joaquim 9
Jefferson Denise 10
```
Note que não são permitidos arquivos com caminhos isolados, isto é, onde ambos os vértices de uma determinada nova aresta não façam parte do grafo.

## Execução
A execução do programa espera uma opção de linha de comando para indicar qual a intenção do usuário, o caminho do arquivo a ser aberto e algumas informações extras dependendo do comando. Exemplo de execução:
```ubuntu
./main.exe --dijkstra "../dados/nomes.csv" "Paulo" "Joaquim"
```
Lista de comandos:
```
--info / -i
        Imprime na tela a quantidade de vertices e de enlaces do grafo.
        Uso: --info <caminho-do-arquivo-do-grafo>
--vertices / -v
        Imprime na tela a lista de vertices do grafo.
        Uso: --vertices <caminho-do-arquivo-do-grafo>
--dijkstra / -D
        Imprime na tela o menor caminho entre dois vertices.
        Uso: --dijkstra <caminho-do-arquivo-do-grafo> <origem> <destino>
--diametro / -d
        Imprime na tela o diametro do grafo.
        Uso: --diametro <caminho-do-arquivo-do-grafo>
--grau / -g
        Imprime na tela o vertice de maior centralidade de grau do grafo.
        Uso: --grau <caminho-do-arquivo-do-grafo>
--intermediacao / -I
        Imprime na tela o vertice com maior centralidade de intermediacao.
        Uso: --intermediacao <caminho-do-arquivo-do-grafo>
--help / -h
        Imprime na tela esta lista de ajuda.
        Uso: --help
```