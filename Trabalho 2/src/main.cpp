/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 27 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <sstream>
#include <utility>
#include <chrono>

#include <getopt.h>
#include <stdlib.h>

#include "./include/configurations.h"
#include "./include/error.h"
#include "./include/Messenger.h"
#include "./include/FileHandler.h"
#include "./include/Vertex.h"
#include "./include/Edge.h"
#include "./include/Graph.h"
#include "./include/Populator.h"
#include "./include/Dijkstra.h"
#include "./include/PairMaker.h"

int
main (int argc, char *argv[])
{
    // Declarações iniciais
    Messenger *messenger = new Messenger;
    Graph *graph = new Graph;
    Populator *populator = new Populator (graph);
    Dijkstra *dijkstra;
    std::vector<std::pair<std::string, std::vector<std::string>>> fileContent;
    FileHandler *fileHandler;

    char option = ' ';
    int optionIndex = 0;

    errorType error = error::ok;
    
    // Checar a quantidade de argumentos
    if (argc > configurations::highestAmountOfArguments || argc < configurations::leastAmountOfArguments){
        messenger->ShowErrorMessage (error::notEnoughArguments);
        messenger->ShowHelpMessage();
        delete messenger;
        delete graph;
        delete populator;
        exit(error::notEnoughArguments);
    } 

    // Ler os argumentos de linha de comando e obter as long_options ou short_options usadas.
    while ((option = getopt_long(argc, argv, configurations::shortOptions.c_str(), configurations::longOptions, &optionIndex)) > -1) {
        switch (option) {
            // Numero de vertices e de enlaces
            case 'i':
                fileHandler = new FileHandler(optarg, ' ');
                if ((error = fileHandler->Open()) != error::ok) {
                    messenger->ShowErrorMessage (error);
                    delete messenger;
                    delete graph;
                    delete populator;
                    exit (error);
                }

                fileHandler->Read(&fileContent);
                fileHandler->Close();
                delete fileHandler;

                // Popular Graph
                if((error = populator->Populate(fileContent)) != error::ok) {
                    messenger->ShowErrorMessage (error);
                    delete messenger;
                    delete graph;
                    delete populator;
                    exit (error);
                }
                messenger->ShowInformations (graph);
            break;

            // Todos os vertices
            case 'v':
                fileHandler = new FileHandler(optarg, ' ');
                if ((error = fileHandler->Open()) != error::ok) {
                    messenger->ShowErrorMessage (error);
                    delete messenger;
                    delete graph;
                    delete populator;
                    exit (error);
                }

                fileHandler->Read(&fileContent);
                fileHandler->Close();
                delete fileHandler;

                // Popular Graph
                if((error = populator->Populate(fileContent)) != error::ok) {
                    messenger->ShowErrorMessage (error);
                    delete messenger;
                    delete graph;
                    delete populator;
                    exit (error);
                }
                messenger->ShowAllVertices (graph);
            break;

            // Dijkstra com dois argumentos adicionais
            case 'D':
                if (argc != configurations::highestAmountOfArguments) {
                    messenger->ShowErrorMessage (error::notEnoughArguments);
                    messenger->ShowHelpMessage();
                    delete messenger;
                    delete graph;
                    delete populator;
                    exit (error::notEnoughArguments);
                }
                fileHandler = new FileHandler(optarg, ' ');
                if ((error = fileHandler->Open()) != error::ok) {
                    messenger->ShowErrorMessage (error);
                    delete messenger;
                    delete graph;
                    delete populator;
                    exit (error);
                }

                fileHandler->Read(&fileContent);
                fileHandler->Close();
                delete fileHandler;

                // Popular Graph
                if((error = populator->Populate(fileContent)) != error::ok) {
                    messenger->ShowErrorMessage (error);
                    delete messenger;
                    delete graph;
                    delete populator;
                    exit (error);
                }

                dijkstra = new Dijkstra (graph);

                if ((error = dijkstra->Calculate(argv[optind], argv[optind + 1])) != error::ok ) {
                    messenger->ShowErrorMessage (error);
                    delete messenger;
                    delete graph;
                    delete populator;
                    delete dijkstra;
                    exit (error);
                }
                
                messenger->ShowDijkstra (dijkstra);
                delete dijkstra;
            break;

            // Diametro
            case 'd':
                fileHandler = new FileHandler(optarg, ' ');
                if ((error = fileHandler->Open()) != error::ok) {
                    messenger->ShowErrorMessage (error);
                    delete messenger;
                    delete graph;
                    delete populator;
                    exit (error);
                }

                fileHandler->Read(&fileContent);
                fileHandler->Close();
                delete fileHandler;

                // Popular Graph
                if((error = populator->Populate(fileContent)) != error::ok) {
                    messenger->ShowErrorMessage (error);
                    delete messenger;
                    delete graph;
                    delete populator;
                    exit (error);
                }

                dijkstra = new Dijkstra (graph);
                messenger->ShowDiameter(dijkstra->GetDiameter());
                delete dijkstra;
            break;


            // Maior centralidade de grau
            case 'g':
                fileHandler = new FileHandler(optarg, ' ');
                if ((error = fileHandler->Open()) != error::ok) {
                    messenger->ShowErrorMessage (error);
                    delete messenger;
                    delete graph;
                    delete populator;
                    exit (error);
                }

                fileHandler->Read(&fileContent);
                fileHandler->Close();
                delete fileHandler;

                // Popular Graph
                if((error = populator->Populate(fileContent)) != error::ok) {
                    messenger->ShowErrorMessage (error);
                    delete messenger;
                    delete graph;
                    delete populator;
                    exit (error);
                }

                messenger->ShowHighestDegree (graph->HighestDegree());
            break;

            // Maior centralidade de intermediação
            case 'I':
                fileHandler = new FileHandler(optarg, ' ');
                if ((error = fileHandler->Open()) != error::ok) {
                    messenger->ShowErrorMessage (error);
                    delete messenger;
                    delete graph;
                    delete populator;
                    exit (error);
                }

                fileHandler->Read(&fileContent);
                fileHandler->Close();
                delete fileHandler;

                // Popular Graph
                if((error = populator->Populate(fileContent)) != error::ok) {
                    messenger->ShowErrorMessage (error);
                    delete messenger;
                    delete graph;
                    delete populator;
                    exit (error);
                }

                dijkstra = new Dijkstra (graph);

                messenger->ShowHighestBetweennessCentrality (dijkstra->HighestBetweennessCentrality());
                delete dijkstra;
            break;

            // Ajuda
            case 'h':
                messenger->ShowHelpMessage();
            break;
            // Opção errada.
            default:
                messenger->ShowErrorMessage (error::wrongOption);
                messenger->ShowHelpMessage();
            break;
        }
    }


    delete messenger;
    delete graph;
    delete populator;
    return error::ok;
}