/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 27 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef ERROR
#define ERROR

/*
 * Namespace que guarda as constantes de erro do programa.
 */
namespace error
{
    const unsigned int ok = 0;
    const unsigned int wrongOption = 1;
    const unsigned int couldNotOpenFile = 2;
    const unsigned int nullFile = 3;
    const unsigned int fileAlreadyClosed = 4;
    const unsigned int couldNotCloseFile = 5;
    const unsigned int noFileName = 6;
    const unsigned int fileAlreadyOpen = 7;
    const unsigned int fileIsNotOpen = 8;
    const unsigned int badFileStream = 9;
    const unsigned int nullEdge = 10;
    const unsigned int notContinuousPath = 11;
    const unsigned int emptyPath = 12;
    const unsigned int invalidContent = 13;
    const unsigned int asymmetricContent = 14;
    const unsigned int nullGraph = 15;
    const unsigned int notAValidPath = 16;
    const unsigned int unconnectedEdge = 17;
    const unsigned int notEnoughArguments = 18;

    const std::string errorMessage[19] = 
    {
        "Ok!",
        "Opção errada!",
        "Não pode abrir o arquivo!",
        "Arquivo nulo!",
        "Arquivo já está fechado!",
        "Não pude fechar o arquivo!",
        "Nome do arquivo não fornecido!",
        "Arquivo já está aberto!",
        "Arquivo não está aberto!",
        "Fluxo de dados corrompido!",
        "Aresta nula!",
        "Aresta fornecida não constrói caminho fechado no grafo!",
        "Caminho nulo!",
        "Conteúdo inválido!",
        "Conteúdo assimétrico!",
        "Grafo nulo!",
        "Não é um caminho válido para o dijkstra!",
        "Aresta isolada das outras!",
        "Numero de argumentos nao e suficiente!"
    };
}

typedef unsigned int errorType;

#endif