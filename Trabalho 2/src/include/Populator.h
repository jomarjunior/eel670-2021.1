/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 27 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef POPULATOR
#define POPULATOR

#include <iostream>
#include <string>
#include <vector>
#include <utility>

#include "Graph.h"

/**
 * Classe que popula um grafo utilizando um vetor de colunas e seus respectivos valores.
 * A classe cria os vértices e insere do grafo.
 */
class Populator
{
    private:
    Graph *graph;

    public:
    /**
     * Construtor da classe Populator, espera a entrada do grafo, já inicializado, que será populado.
     * @param graph o grafo a ser alimentado.
     */
    Populator (Graph *);

    /**
     * Método que executa o algoritmo de população no grafo fornecido no construtor da classe.
     * @param content vetor de pares de (<string> cabeçalho, <vetor de strings> valores da coluna).
     * @return código de erro equivalente ou error::ok caso sucesso.
     */
    errorType
    Populate (std::vector<std::pair<std::string, std::vector<std::string>>>);

};

#endif