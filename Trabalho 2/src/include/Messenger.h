/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 27 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef MESSENGER
#define MESSENGER

#include <iostream>
#include <string>

#include "Graph.h"
#include "Dijkstra.h"

#include "error.h"

/**
 * Classe responsável por realizar impressões de texto e informações na tela.
 */
class Messenger
{
    private:

    public:

    /**
     * Exibe na tela a mensagem relacionada a um erro de execução.
     * @param error Erro encontrado.
     */
    void
    ShowErrorMessage(errorType);

    /*
    * Exibe na tela uma mensagem contendo todos os comandos aceitos pelo programa.
    */
    void
    ShowHelpMessage();

    /**
     * Exibe na tela uma mensagem contendo a quantidade de vértices e enlaces do grafo.
     * @param graph o Grafo o qual as informações serão exibidas.
     */
    void
    ShowInformations(Graph *);
    
    /**
     * Exibe na tela uma mensagem contendo todos os vértices do grafo.
     * @param graph o Grafo o qual os vértices serão exibidos.
     */
    void
    ShowAllVertices(Graph *);

    /**
     * Exibe na tela uma mensagem contendo as informações do algoritmo de dijkstra anteriormente calculado.
     * @param dijkstra o algoritmo de dijkstra com o caminho a ser exibido.
     */
    void
    ShowDijkstra(Dijkstra *);

    /**
     * Exibe na tela uma mensagem contendo as informações do diametro do grafo.
     * @param diameter o Path * que será mostrado na tela.
     */
    void
    ShowDiameter(Path *);

    /**
     * Exibe na tela uma mensagem contendo o vertice com maior centralidade de grau.
     * @param highestDegree O vértice de maior centralidade de grau.
     */
    void
    ShowHighestDegree(Vertex *);

    /**
     * Exibe na tela uma mensagem contendo o vertice com maior centralidade de intermediação.
     * @param highestBetweennessCentralityVertex o Vértice de maior centralidade de intermediação a ser exibido.
     */
    void
    ShowHighestBetweennessCentrality(Vertex *);
};

#endif