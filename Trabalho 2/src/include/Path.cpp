/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 27 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#include "Path.h"

#include <iostream>
#include <string>
#include <vector>

#include "error.h"

/**
 * Destrutor da classe do caminho. Deleta todas as arestas.
 */
Path::~Path ()
{
    for (Edge *edge : this->edges) {
        delete edge;
    }
}

/**
 * Método que insere uma aresta no caminho.
 * @param newEdge a aresta, já inicializada, a ser adicionada.
 * @return código de erro equivalente ou error::ok caso sucesso.
 */
errorType
Path::InsertEdge (Edge *newEdge)
{
    if (! newEdge ) return error::nullEdge;
    if (!this->edges.empty()) {
        if (this->edges.back()->GetSecondVertex()->GetLabel() != newEdge->GetFirstVertex()->GetLabel()) return error::notContinuousPath;
    }

    this->edges.push_back(newEdge);

    return error::ok;
}

/**
 * Calcula o custo do caminho.
 * @param result <saída> O resultado do cálculo do custo.
 * @return o código de erro equivalente ou error::ok caso sucesso.
 */
errorType
Path::CalculateCost (double *result)
{
    *result = 0;
    if (this->edges.size() == 0) return error::emptyPath;

    for (Edge *edge : this->edges) {
        *result += edge->GetWeight();
    }

    return error::ok;
}

/**
 * Mostra na tela a sequência de vértices do caminho, junto com o peso das arestas que os ligam.
 * @return o código de error equivalente ou error::ok caso sucesso.
 */
errorType
Path::PrintVertexSequence ()
{
    if (this->edges.size() == 0) return error::emptyPath;

    for (Edge *edge : this->edges) {
        std::cout << edge->GetFirstVertex()->GetLabel() << "--" << edge->GetWeight() << "--";
    }
    std::cout << this->edges.back()->GetSecondVertex()->GetLabel() << std::endl;

    return error::ok;
}

/**
 * Retorna as arestas do caminho.
 * @return vetor de Edge *.
 */
std::vector<Edge *>
Path::GetEdges ()
{
    return this->edges;
}

/**
 * Limpa o vetor de arestas do caminho.
 * @return código de erro equivalente ou error::ok caso sucesso.
 */
errorType
Path::Erase ()
{
    edges.clear();
    return error::ok;
}