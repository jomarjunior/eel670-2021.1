/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 27 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#include "Populator.h"

#include <iostream>
#include <string>
#include <vector>
#include <utility>

#include "Graph.h"
#include "Edge.h"
#include "error.h"

/**
 * Construtor da classe Populator, espera a entrada do grafo, já inicializado, que será populado.
 * @param graph o grafo a ser alimentado.
 */
Populator::Populator (Graph *graph)
{
    this->graph = graph;
}

/**
 * Método que executa o algoritmo de população no grafo fornecido no construtor da classe.
 * @param content vetor de pares de (<string> cabeçalho, <vetor de strings> valores da coluna).
 * @return código de erro equivalente ou error::ok caso sucesso.
 */
errorType
Populator::Populate (std::vector<std::pair<std::string, std::vector<std::string>>> content)
{
    errorType error = error::ok;
    if (!this->graph) return error::nullGraph;
    if (content.size() != 3) return error::invalidContent;
    if ((content.at(0).second.size() != content.at(1).second.size()) || (content.at(0).second.size() != content.at(2).second.size())) return error::asymmetricContent;

    for (unsigned int index = 0; index < content.at(0).second.size(); index++) {
        double weight = std::stod(content.at(2).second.at(index));
        Edge *newEdge = new Edge(content.at(0).second.at(index), content.at(1).second.at(index), weight);

        if ((error = graph->InsertEdge(newEdge)) != error::ok) return error;
    }

    return error::ok;
}