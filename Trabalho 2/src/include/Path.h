/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 27 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef PATH
#define PATH

#include <iostream>
#include <vector>

#include "Edge.h"
#include "error.h"

/**
 * Classe que implementa um caminho único e sem ramificações de arestas e seus vértices.
 */
class Path
{
    private:
    std::vector<Edge *> edges;

    public:
    /**
     * Destrutor da classe do caminho. Deleta todas as arestas.
     */
    ~Path();

    /**
     * Método que insere uma aresta no caminho.
     * @param newEdge a aresta, já inicializada, a ser adicionada.
     * @return código de erro equivalente ou error::ok caso sucesso.
     */
    errorType
    InsertEdge (Edge *);

    /**
     * Calcula o custo do caminho.
     * @param result <saída> O resultado do cálculo do custo.
     * @return o código de erro equivalente ou error::ok caso sucesso.
     */
    errorType
    CalculateCost (double *);

    /**
     * Mostra na tela a sequência de vértices do caminho, junto com o peso das arestas que os ligam.
     * @return o código de error equivalente ou error::ok caso sucesso.
     */
    errorType
    PrintVertexSequence ();

    /**
     * Retorna as arestas do caminho.
     * @return vetor de Edge *.
     */
    std::vector<Edge *>
    GetEdges ();

    /**
     * Limpa o vetor de arestas do caminho.
     * @return código de erro equivalente ou error::ok caso sucesso.
     */
    errorType
    Erase ();
};

#endif