/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 27 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#include "Graph.h"

#include <iostream>
#include <string>
#include <vector>
#include "Edge.h"
#include "Vertex.h"
#include "error.h"

/**
 * Destrutor da classe Graph
 */
Graph::~Graph ()
{
    for (Edge *edge : this->edges) delete edge;
}

/**
 * Insere a aresta em ordem alfabética do seu primeiro vértice. 
 * Caso hajam vértices primários coincidentes, a ordenação se vira para o vértice secundário.
 * @param newEdge Nova aresta a ser adicionada.
 * @return Constante de erro correspondente ou error::ok caso não haja.
 */
errorType
Graph::InsertEdge (Edge *newEdge)
{
    bool inserted = false;

    // Caso vazio, inserir no final.
    if (this->edges.size() == 0) {
        this->edges.push_back (newEdge);
        inserted = true;
    } else {
        // Checar se a aresta a ser adicionada está conectada ao grafo
        // Uma aresta só estará conectada caso um de seus vértices faça parte da lista de vértices do grafo.
        bool unconnectedEdge = true;
        for (Vertex *vertex : this->GetVertices()) {
            if (vertex->GetLabel().compare(newEdge->GetFirstVertex()->GetLabel()) == 0) {
                unconnectedEdge = false;
                break;
            }
            if (vertex->GetLabel().compare(newEdge->GetSecondVertex()->GetLabel()) == 0) {
                unconnectedEdge = false;
                break;
            }
        }
        if (unconnectedEdge) return error::unconnectedEdge;
    }

    std::vector<Edge *>::iterator edgeIterator;
    // Iterar sobre o vetor para encontrar o lugar a ser inserido.
    
    for (
            edgeIterator = this->edges.begin();
            edgeIterator < this->edges.end();
            edgeIterator++
        ) {
            // Caso já tenha sido inserido, interromper o looping.
            if (inserted) break;

            // Checar o local para inserir a aresta dependendo dos rótulos dos vértices de origem.
            // Ordem alfabetica do rótulo do primeiro vértice.
            if ((*edgeIterator)->GetFirstVertex()->GetLabel().compare(newEdge->GetFirstVertex()->GetLabel()) > 0) {
                this->edges.insert(edgeIterator, newEdge);
                inserted = true;
            } else
            // Caso haja coincidência de vértice primário, basear-se no rótulo do vértice secundário.
            if ((*edgeIterator)->GetFirstVertex()->GetLabel().compare(newEdge->GetFirstVertex()->GetLabel()) == 0) {
                if ((*edgeIterator)->GetSecondVertex()->GetLabel().compare(newEdge->GetSecondVertex()->GetLabel()) >= 0) {
                    this->edges.insert(edgeIterator, newEdge);
                    inserted = true;
                }
            }
    }

    // Caso não encontre lugar no meio, deve ser inserido no final.
    if (!inserted){
        this->edges.push_back(newEdge);
        inserted = true;
    }

    return error::ok;
}

/**
 * Obtém a lista de arestas do grafo.
 * @return Vetor de Edge * do grafo.
 */
std::vector<Edge *>
Graph::GetEdges ()
{
    return this->edges;
}

/**
 * Obtém a lista de vértices do grafo
 * @return Vetor de Vertex * do grafo.
 */
std::vector<Vertex *>
Graph::GetVertices ()
{
    std::vector<Vertex *> vertices;
    // Iterar nas arestas do grafo
    for (Edge *edge : this->edges) {
        // Caso vertices esteja vazio, adicionar imediatamente os valores dos vertices
        // Imagina-se que não hajam arestas com vertices internos iguais
        if (vertices.size() == 0) {
            vertices.push_back(edge->GetFirstVertex());
            vertices.push_back(edge->GetSecondVertex());
        } else {
            // Checar na lista de vertices se já existe os vertices a serem adicionados.
            bool alreadyExists[2] = {false, false};
            std::vector<Vertex *>::iterator vertexIterator;

            // Iterar na lista de vertices
            for (vertexIterator = vertices.begin(); vertexIterator < vertices.end(); vertexIterator++) {
                // Caso o primeiro vertice exista
                if ((*vertexIterator)->GetLabel().compare(edge->GetFirstVertex()->GetLabel()) == 0) {
                    alreadyExists[0] = true;
                }
                // Caso o segundo vertice exista
                if ((*vertexIterator)->GetLabel().compare(edge->GetSecondVertex()->GetLabel()) == 0) {
                    alreadyExists[1] = true;
                }
                // Caso ambos existam, interromper o looping pois não há necessidade de mais checagem
                if (alreadyExists[0] && alreadyExists[1]) break;
            }
            // Adicionar os vertices que não foram encontrados
            if (!alreadyExists[0]) {
                vertices.push_back(edge->GetFirstVertex());
            }
            if (!alreadyExists[1]) {
                vertices.push_back(edge->GetSecondVertex());
            }
        }
    }
    return vertices;
}

/**
 * Obtém o vértice de maior centralidade de grau do grafo.
 * @return O vértice que faz parte do maior número de arestas.
 */
Vertex *
Graph::HighestDegree ()
{
    unsigned int  count = 0, highestCount = 0;
    Vertex *highestDegree;
    for (Vertex *vertex : this->GetVertices()) {
        count = 0;
        for (Edge *edge : this->GetEdges()) {
            if (vertex->GetLabel().compare(edge->GetFirstVertex()->GetLabel()) == 0) {
                count++;
                continue;
            }
            if (vertex->GetLabel().compare(edge->GetSecondVertex()->GetLabel()) == 0) {
                count++;
                continue;
            }
        }
        if (highestCount < count) {
            highestCount = count;
            highestDegree = vertex;
        }
    }

    return highestDegree;
}