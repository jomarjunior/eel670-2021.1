/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 27 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#include "Vertex.h"

#include <iostream>
#include <string>
#include <math.h>

#include "error.h"

/**
 * Construtor da classe Vertex.
 * @param label Rótulo do vértice.
 */
Vertex::Vertex (std::string label)
{
    this->label = label;
    this->id = id;
}

/**
 * Muda o id do vértice.
 * @param newId novo id para o vértice.
 * @return código de erro equivalente ou error::ok caso sucesso.
 */
errorType
Vertex::SetId (unsigned long newId)
{
    this->id = newId;
    return error::ok;
}

/**
 * Retorna o rótulo do vértice.
 * @return O rótulo do vértice.
 */
std::string
Vertex::GetLabel ()
{
    return this->label;
}

/**
 * Retorna o id do vértice.
 * @return O id do vértice.
 */
unsigned long
Vertex::GetId ()
{
    return this->id;
}