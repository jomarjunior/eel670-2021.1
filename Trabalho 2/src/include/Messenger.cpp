/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 27 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#include "Messenger.h"

#include <iostream>
#include <string>
#include <vector>

#include "configurations.h"
#include "Graph.h"
#include "error.h"

/**
 * Exibe na tela a mensagem relacionada a um erro de execução.
 * @param error Erro encontrado.
 */
void
Messenger::ShowErrorMessage ( errorType error )
{
    std::cout << "---- Erro detectado! ----" << std::endl;
    std::cout << "Codigo " << error << " - " << error::errorMessage[error] << std::endl;
    std::cout << "-------------------------" << std::endl;
}

/*
 * Exibe na tela uma mensagem contendo todos os comandos aceitos pelo programa.
 */
void
Messenger::ShowHelpMessage ()
{
    std::cout << "Comandos aceitos: \n\n";
    std::cout << "--info / -i\n\tImprime na tela a quantidade de vertices e de enlaces do grafo.\n\tUso: --info <caminho-do-arquivo-do-grafo>";
    std::cout << std::endl;
    std::cout << "--vertices / -v\n\tImprime na tela a lista de vertices do grafo.\n\tUso: --vertices <caminho-do-arquivo-do-grafo>";
    std::cout << std::endl;
    std::cout << "--dijkstra / -D\n\tImprime na tela o menor caminho entre dois vertices.\n\tUso: --dijkstra <caminho-do-arquivo-do-grafo> <origem> <destino>";
    std::cout << std::endl;
    std::cout << "--diametro / -d\n\tImprime na tela o diametro do grafo.\n\tUso: --diametro <caminho-do-arquivo-do-grafo>";
    std::cout << std::endl;
    std::cout << "--grau / -g\n\tImprime na tela o vertice de maior centralidade de grau do grafo.\n\tUso: --grau <caminho-do-arquivo-do-grafo>";
    std::cout << std::endl;
    std::cout << "--intermediacao / -I\n\tImprime na tela o vertice com maior centralidade de intermediacao.\n\tUso: --intermediacao <caminho-do-arquivo-do-grafo>";
    std::cout << std::endl;
    std::cout << "--help / -i\n\tImprime na tela esta lista de ajuda.\n\tUso: --help";
    std::cout << std::endl;
    
}

/**
 * Exibe na tela uma mensagem contendo a quantidade de vértices e enlaces do grafo.
 * @param graph o Grafo o qual as informações serão exibidas.
 */
void
Messenger::ShowInformations (Graph *graph)
{
    std::cout << "Quantidade de Vertices: " << graph->GetVertices().size() << std::endl;
    std::cout << "Quantidade de Enlaces: " << graph->GetEdges().size() << std::endl;
}

/**
 * Exibe na tela uma mensagem contendo todos os vértices do grafo.
 * @param graph o Grafo o qual os vértices serão exibidos.
 */
void
Messenger::ShowAllVertices (Graph *graph)
{
    std::cout << "Todos os vertices do grafo" << std::endl;
    for (Vertex *vertex : graph->GetVertices()) {
        std::cout << "[" << vertex->GetLabel() << "] ";
    }
    std::cout << std::endl;
}

/**
 * Exibe na tela uma mensagem contendo as informações do algoritmo de dijkstra anteriormente calculado.
 * @param dijkstra o algoritmo de dijkstra com o caminho a ser exibido.
 */
void
Messenger::ShowDijkstra (Dijkstra *dijkstra)
{
    std::cout << "=======Dijkstra=======" << std::endl;
    std::cout << "--Caminho de menor peso entre os alvos--" << std::endl;
    dijkstra->GetLeastCostPath()->PrintVertexSequence();
    std::cout << "Peso total: " << dijkstra->GetLeastCost() << std::endl;
}

/**
 * Exibe na tela uma mensagem contendo as informações do diametro do grafo.
 * @param diameter o Path * que será mostrado na tela.
 */
void
Messenger::ShowDiameter (Path *diameter)
{
    std::cout << "Diametro do grafo" << std::endl;
    diameter->PrintVertexSequence();
}

/**
 * Exibe na tela uma mensagem contendo o vertice com maior centralidade de grau.
 * @param highestDegree O vértice de maior centralidade de grau.
 */
void
Messenger::ShowHighestDegree (Vertex *highestDegree)
{
    std::cout << "Vertice com maior centralidade de grau:" << std::endl;
    std::cout << highestDegree->GetLabel() << std::endl;
}

/**
 * Exibe na tela uma mensagem contendo o vertice com maior centralidade de intermediação.
 * @param highestBetweennessCentralityVertex o Vértice de maior centralidade de intermediação a ser exibido.
 */
void
Messenger::ShowHighestBetweennessCentrality (Vertex *highestBetweennessCentralityVertex)
{
    std::cout << "Vertice com maior centralidade de intermediacao:" << std::endl;
    std::cout << highestBetweennessCentralityVertex->GetLabel() << std::endl;
}