/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 27 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef CONFIGURATIONS   
#define CONFIGURATIONS

#include <iostream>
#include <string>
#include <getopt.h>

/*
 * Namespace que guarda constantes de configuração do programa.
 */
namespace configurations
{
    // Main.cpp
    const struct option longOptions[] = {
        {"info", required_argument, 0, 'i'},
        {"vertices", required_argument, 0, 'v'},
        {"dijkstra", required_argument, 0, 'D'},
        {"diametro", required_argument, 0, 'd'},
        {"grau", required_argument, 0, 'g'},
        {"intermediacao", required_argument, 0, 'I'},
        {"help", no_argument, 0, 'h'},
        {0, 0, 0, 0}
    };
    const std::string shortOptions = "i:v:D:d:g:I:";
    const int leastAmountOfArguments = 2;
    const int highestAmountOfArguments = 5;
}

#endif