/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 27 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#include "PairMaker.h"

#include <iostream>
#include <vector>
#include <utility>

#include "Vertex.h"

/**
 * Cria todos os pares possíveis de um vetor. Não considera posições. (A,B = B,A)
 * @param data vetor de valores o qual os pares serão montados
 * @return vetor de todos os pares possíveis dentro do vetor data.
 */
std::vector<std::pair<Vertex *, Vertex *>>
PairMaker::MakePairs (std::vector<Vertex *> data)
{
    std::vector<std::pair<Vertex *, Vertex *>> result;
    unsigned int leftIndex, rightIndex;

    for (leftIndex = 0; leftIndex < data.size(); leftIndex++) {
        for (rightIndex = leftIndex + 1; rightIndex < data.size(); rightIndex++) {
            result.push_back(std::make_pair(data.at(leftIndex), data.at(rightIndex)));
        }
    }

    return result;
}