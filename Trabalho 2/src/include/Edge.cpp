/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 27 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#include "Edge.h"

#include <iostream>
#include <string>
#include <utility>
#include <math.h>

#include "Vertex.h"

/**
 * Construtor da classe Edge.
 * @param firstLabel Rótulo do primeiro vertice da aresta.
 * @param secondLabel Rótulo do segundo vertice da aresta.
 * @param weight Peso da aresta.
 */
Edge::Edge (std::string firstLabel, std::string secondLabel, double weight)
{
    this->weight = weight;
    this->vertices.first = new Vertex (firstLabel);
    this->vertices.second = new Vertex (secondLabel);
}

/**
 * Destrutor da classe Edge.
 */
Edge::~Edge ()
{
    delete this->vertices.first;
    delete this->vertices.second;
}

/**
 * Método getter do primeiro vértice da aresta.
 * @return O primeiro vértice da aresta.
 */
Vertex *
Edge::GetFirstVertex ()
{
    return this->vertices.first;
}

/**
 * Método getter do segundo vértice da aresta.
 * @return O segundo vértice da aresta.
 */
Vertex *
Edge::GetSecondVertex ()
{
    return this->vertices.second;
}

/**
 * Método getter do peso da aresta.
 * @return O peso da aresta.
 */
double
Edge::GetWeight ()
{
    return this->weight;
}

/**
 * Método setter do peso da aresta.
 * @param newWeight novo peso para a aresta.
 * @return Erro associado ou error::ok caso não hajam erros.
 */
errorType
Edge::SetWeight (double newWeight)
{
    this->weight = newWeight;
    return error::ok;
}