/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 27 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef PAIRMAKER
#define PAIRMAKER

#include <iostream>
#include <utility>
#include <vector>

#include "Vertex.h"

/**
 * Classe estática resposável por gerar todos os pares possíveis de uma lista de vértices.
 */
class PairMaker
{
    private:

    public:

    /**
     * Cria todos os pares possíveis de um vetor. Não considera posições. (A,B = B,A)
     * @param data vetor de valores o qual os pares serão montados
     * @return vetor de todos os pares possíveis dentro do vetor data.
     */
    static std::vector<std::pair<Vertex *, Vertex *>>
    MakePairs (std::vector<Vertex *>);
};

#endif