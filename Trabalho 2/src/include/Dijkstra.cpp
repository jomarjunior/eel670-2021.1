/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 27 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#include "Dijkstra.h"

#include <iostream>
#include <string>
#include <vector>
#include <utility>
#include <cmath>
#include <queue>

#include "error.h"
#include "Graph.h"
#include "Path.h"
#include "Edge.h"
#include "Vertex.h"
#include "PairMaker.h"

/**
 * Construtor da classe Dijkstra
 * @param graph Grafo o qual será executado o algoritmo.
 */
Dijkstra::Dijkstra (Graph *graph)
{
    this->graph = graph;
    this->adjacencyList = new std::list<std::pair<unsigned int, double>> [this->graph->GetVertices().size()];

    unsigned long index = 0;
    for (Vertex *vertex : this->graph->GetVertices()) {
        this->labelList.push_back(vertex->GetLabel());
        for (Edge *edge : this->graph->GetEdges()) {
            if (edge->GetFirstVertex()->GetLabel().compare(vertex->GetLabel()) == 0) {
                edge->GetFirstVertex()->SetId(index);
            }
            if (edge->GetSecondVertex()->GetLabel().compare(vertex->GetLabel()) == 0) {
                edge->GetSecondVertex()->SetId(index);
            }
        }
        index++;
    }
}

Dijkstra::~Dijkstra ()
{
    if (this->leastCostPath) delete this->leastCostPath;
}

/**
 * Gera a lista de adjacências do grafo fornecido pelo construtor.
 * @return Erro encontrado ou error::ok em caso de sucesso.
 */
errorType
Dijkstra::_GenerateAdjacencyList ()
{
    if (!this->graph) return error::nullGraph;

    for (Vertex *vertex : this->graph->GetVertices()) {
        for (Edge *edge : this->graph->GetEdges()) {
            if (edge->GetFirstVertex()->GetLabel().compare(vertex->GetLabel()) == 0) {
                this->adjacencyList[vertex->GetId()].push_back(std::make_pair(edge->GetSecondVertex()->GetId(), edge->GetWeight()));
            } else
            if (edge->GetSecondVertex()->GetLabel().compare(vertex->GetLabel()) == 0){
                this->adjacencyList[vertex->GetId()].push_back(std::make_pair(edge->GetFirstVertex()->GetId(), edge->GetWeight()));
            }
        }
    }
    return error::ok;
}

/**
 * Executa o algoritmo de Dijkstra no grafo fornecido pelo construtor e guarda o resultado em atributos internos.
 * @param from vértice de partida
 * @param to vértice de chegada
 */
errorType
Dijkstra::Calculate (std::string from, std::string to)
{
    unsigned long pathIds[2];
    unsigned long verticesAmount = this->graph->GetVertices().size();
    double weights[verticesAmount];
    long long previousVertex[verticesAmount];
    bool visitedVertices[verticesAmount];
    std::priority_queue<std::pair<unsigned long, double>> priorityQueue;
    errorType error = error::ok;

    // Gerar a lista de adjacencias e tratar os erros
    if ((error = this->_GenerateAdjacencyList()) != error::ok) return error;

    // Transformar as strings com nomes dos vértices em seus respectivos números de Id para facilitar o algoritmo.
    bool foundIds[2] = {false, false};
    for (unsigned long index = 0; index < this->labelList.size(); index++) {
        if (this->labelList[index].compare(from) == 0) {
            pathIds[0] = index;
            foundIds[0] = true;
        }
        if (this->labelList[index].compare(to) == 0) {
            pathIds[1] = index;
            foundIds[1] = true;
        }
    }
    // Caso não encontre um e/ou o outro, retornar erro de caminho inválido
    if (! (foundIds[0] && foundIds[1])) return error::notAValidPath;

    // Definir os pesos iniciais, a lista de visitados e a lista de vertices anteriores.
    for (unsigned long index = 0; index < verticesAmount; index++) {
        weights [index] = INFINITY;
        visitedVertices [index] = false;
        previousVertex [index] = -1;
    }
    // Corrigir o peso para o vértice de origem
    weights [pathIds[0]] = 0;
    // Adicionar na fila o vértice de origem
    priorityQueue.push(std::make_pair(pathIds[0], weights [pathIds[0]]));

    // Iterar na fila
    while (! priorityQueue.empty() ) {
        // Sacar o pair no topo da lista
        std::pair <unsigned long, double> way = priorityQueue.top();
        priorityQueue.pop();
        // Caso não tenha sido visitado o vértice do sub caminho
        if (! visitedVertices[way.first]) {
            // Define-o como visitado
            visitedVertices[way.first] = true;
            // Iterar na lista de adjacências do vértice a ser observado
            std::list<std::pair<unsigned int, double>>::iterator adjacencyListIterator;
            for (
                adjacencyListIterator = this->adjacencyList[way.first].begin();
                adjacencyListIterator != this->adjacencyList[way.first].end();
                adjacencyListIterator++
                ) {
                    // Alterar o valor do peso caso este seja menor ao que já esta guardado
                    if (weights[adjacencyListIterator->first] > (weights[way.first] + adjacencyListIterator->second)) {
                        weights[adjacencyListIterator->first] = weights[way.first] + adjacencyListIterator->second;
                        // Guardar o vértice anterior
                        previousVertex[adjacencyListIterator->first] = (unsigned long) way.first;
                        // Adicionar à fila para posterior análise
                        priorityQueue.push(std::make_pair(adjacencyListIterator->first, weights[adjacencyListIterator->first]));
                    }
                }
        }
    }
    // Atualizar o menor custo.
    this->leastCost = weights[pathIds[1]];

    // Obter o caminho de menor custo apartir do cálculo anterior
    bool finished = false;
    std::vector<Edge *> edgeList;
    unsigned int target = pathIds[1];
    // Iterar na previousVertex de trás para frente até retornarmos ao vértice de origem.
    while (!finished) {
        // Montar arestas com os vértices da lista de vértices anteriores
        unsigned int auxiliar = target;
        target = previousVertex[auxiliar];
        edgeList.push_back(new Edge(this->labelList[target], this->labelList[auxiliar], weights[auxiliar]));
        if (target == pathIds[0]) finished = true;
    }
    this->leastCostPath = new Path();

    // Montar o caminho Path * com os vértices criados anteriormente de trás para frente, corrigindo os pesos acumulados.
    double accumulatedWeight = 0;
    for (int index = edgeList.size() - 1; index >= 0; index--) {
        // Remover o acúmulo de peso
        edgeList.at(index)->SetWeight(edgeList.at(index)->GetWeight() - accumulatedWeight);
        accumulatedWeight += edgeList.at(index)->GetWeight();
        // Inserir a aresta ao menor caminho
        this->leastCostPath->InsertEdge(edgeList.at(index));
    }

    return error::ok;
}

/**
 * Obter o caminho com o menor custo calculado em Dijkstra::Calculate()
 * @return o Path * com o caminho de menor custo.
 */
Path *
Dijkstra::GetLeastCostPath ()
{
    return this->leastCostPath;
}

/**
 * Obtém o menor custo entre os vértices fornecidos em Dijkstra::Calculate()
 * @return O menor custo.
 */
double
Dijkstra::GetLeastCost ()
{
    double result = 0;
    for (Edge *edge : this->leastCostPath->GetEdges()) {
        result += edge->GetWeight();
    }
    return result;
}

/**
 * Obtém o diâmetro do grafo alimentado ao Dijkstra.
 * @return O caminho de menor custo com o maior custo
 */
Path *
Dijkstra::GetDiameter ()
{
    std::vector<Path *> paths;
    unsigned int diameter = 0;
    double diameterCost = 0;
    double pairCost = 0;
    for (std::pair<Vertex *, Vertex *> pair : PairMaker::MakePairs(graph->GetVertices())) {
        this->Calculate(pair.first->GetLabel(), pair.second->GetLabel());
        if (!paths.empty()) {
            paths.at(diameter)->CalculateCost(&diameterCost);
            this->GetLeastCostPath()->CalculateCost(&pairCost);
            if (diameterCost < pairCost) {
                // O caminho a ser adicionado no final tem o mesmo indice que o tamanho do vector antes dele ser adicionado
                diameter = paths.size();
            }
        }

        paths.push_back(this->GetLeastCostPath());
    }

    return paths.at(diameter);
}

/**
 * Obtém o vértice de maior centralidade de intermediação do grafo alimentado ao Dijkstra
 * @return O vértice que faz parte do maior número de caminhos de menores custos.
 */
Vertex *
Dijkstra::HighestBetweennessCentrality ()
{
    std::vector<Path *> paths;
    unsigned int count = 0, highestBetweennessCentrality = 0;
    Vertex *highest;

    for (std::pair<Vertex *, Vertex *> pair : PairMaker::MakePairs(this->graph->GetVertices())) {
        this->Calculate(pair.first->GetLabel(), pair.second->GetLabel());
        paths.push_back(this->GetLeastCostPath());
    }
    for (Vertex *vertex : this->graph->GetVertices()) {
        count = 0;
        for (Path *path : paths) {
            for (Edge *edge : path->GetEdges()) {
                if (vertex->GetLabel().compare (edge->GetFirstVertex()->GetLabel()) == 0) {
                    count++;
                }
                if (vertex->GetLabel().compare (edge->GetSecondVertex()->GetLabel()) == 0) {
                    count++;
                }
            }
        }
        if (count > highestBetweennessCentrality) {
            highestBetweennessCentrality = count;
            highest = vertex;
        }
    }

    return highest;
}