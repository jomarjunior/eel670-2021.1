/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 27 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef GRAPH
#define GRAPH

#include <iostream>
#include <vector>
#include "Edge.h"
#include "Vertex.h"
#include "error.h"

/**
 * Classe que implementa o grafo simétrico utilizando as classes Vertex e Edge.
 */
class Graph
{
    private:
    std::vector<Edge *> edges;

    public:
    /**
     * Destrutor da classe Graph
     */
    ~Graph();

    /**
     * Insere a aresta em ordem alfabética do seu primeiro vértice. 
     * Caso hajam vértices primários coincidentes, a ordenação se vira para o vértice secundário.
     * @param newEdge Nova aresta a ser adicionada.
     * @return Constante de erro correspondente ou error::ok caso não haja.
     */
    errorType
    InsertEdge (Edge *);

    /**
     * Obtém a lista de arestas do grafo.
     * @return Vetor de Edge * do grafo.
     */
    std::vector<Edge *>
    GetEdges ();

    /**
     * Obtém a lista de vértices do grafo
     * @return Vetor de Vertex * do grafo.
     */
    std::vector<Vertex *>
    GetVertices();

    /**
     * Obtém o vértice de maior centralidade de grau do grafo.
     * @return O vértice que faz parte do maior número de arestas.
     */
    Vertex *
    HighestDegree ();
};

#endif