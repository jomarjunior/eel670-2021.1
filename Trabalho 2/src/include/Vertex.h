/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 27 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef VERTEX
#define VERTEX

#include <iostream>
#include <string>

#include "error.h"

/**
 * Classe que implementa os vértices de um grafo.
 */
class Vertex
{
    private:
    std::string label;
    unsigned long id;

    /**
     * Construtor da classe Vertex.
     * @param label Rótulo do vértice.
     */
    public:
    Vertex (std::string);

    /**
     * Muda o id do vértice.
     * @param newId novo id para o vértice.
     * @return código de erro equivalente ou error::ok caso sucesso.
     */
    errorType
    SetId (unsigned long);

    /**
     * Retorna o rótulo do vértice.
     * @return O rótulo do vértice.
     */
    std::string
    GetLabel ();

    /**
     * Retorna o id do vértice.
     * @return O id do vértice.
     */
    unsigned long
    GetId ();
};

#endif