/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 27 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef EDGE
#define EDGE

#include <iostream>
#include <string>
#include <utility>

#include "Vertex.h"

/**
 * Classe que implementa as arestas de um grafo utilizando um par da classe Vertex
 */
class Edge
{
    private:
    std::pair<Vertex *, Vertex *> vertices;
    double weight;

    public:
    /**
     * Construtor da classe Edge.
     * @param firstLabel Rótulo do primeiro vertice da aresta.
     * @param secondLabel Rótulo do segundo vertice da aresta.
     * @param weight Peso da aresta.
     */
    Edge (std::string, std::string, double = 1);

    /**
     * Destrutor da classe Edge.
     */
    ~Edge ();

    /**
     * Método getter do primeiro vértice da aresta.
     * @return O primeiro vértice da aresta.
     */
    Vertex *
    GetFirstVertex ();

    /**
     * Método getter do segundo vértice da aresta.
     * @return O segundo vértice da aresta.
     */
    Vertex *
    GetSecondVertex ();

    /**
     * Método getter do peso da aresta.
     * @return O peso da aresta.
     */
    double
    GetWeight ();

    /**
     * Método setter do peso da aresta.
     * @param newWeight novo peso para a aresta.
     * @return Erro associado ou error::ok caso não hajam erros.
     */
    errorType
    SetWeight (double);
};

#endif