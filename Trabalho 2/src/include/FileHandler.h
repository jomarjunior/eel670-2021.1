/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 27 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef FILE_HANDLER
#define FILE_HANDLER

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <sstream>
#include <utility>

#include "error.h"

/**
 * Classe que implementa o leitor de arquivos.
 */
class FileHandler
{
    private:
    std::string fileName;
    std::ifstream *file = NULL;
    char sepparator;

    public:

    /**
     * Construtor do controlador de arquivos, recebe o arquivo a ser lido.
     * @param file arquivo a ser lido com seu caminho relativo ou completo.
     */
    FileHandler (std::string, char = ',');

    /**
     * Destrutor da classe, responsável por checar se o arquivo está aberto e fecha-lo, além de liberar a memória dos seus ponteiros.
     */
    ~FileHandler ();

    /**
     * Abre o arquivo associado ao controlador de arquivos.
     * @return O código de erro associado ao problema encontrado ou error::ok caso não haja.
     */
    errorType
    Open ();

    /**
     * Fecha o arquivo aberto anteriormente pela FileHandler::Open();
     * @return O código de erro associado ao problema encontrado ou error::ok caso não haja.
     */
    errorType
    Close ();

    /**
     * Lê o arquivo anteriormente aberto e devolve um vetor de pares.
     * @param result <saída> vetor de pares com a coluna seguida de um vetor de seus valores em string.
     * @param haveHeader indica a existência ou não de um cabeçalho no arquivo (primeira linha não contém dados, mas o nome das colunas)
     * @return O código de erro associado ao problema encontrado ou error::ok caso não haja. 
     */
    errorType
    Read (std::vector<std::pair<std::string, std::vector<std::string>>> *, bool = false);
};

#endif