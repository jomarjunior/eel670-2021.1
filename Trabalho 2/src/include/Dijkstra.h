/*
 * UNIVERSIDADE FEDERAL DO RIO DE JANEIRO
 * ESCOLA POLITÉCNICA
 * DEPARTAMENTO DE ENGENHARIA ELETRÔNICA E DE COMPUTAÇÃO
 * 27 DE AGOSTO DE 2021
 * EEL670 - 2021.1
 * JOMAR JÚNIOR DE SOUZA PEREIRA
 */

#ifndef DIJKSTRA
#define DIJKSTRA

#include "Graph.h"

#include <iostream>
#include <string>
#include <vector>
#include <list>

#include "error.h"
#include "Path.h"

/**
 * Classe responsável por aplicar o algoritmo de dijkstra e todos os cálculos envolvendo este.
 */
class Dijkstra
{
    private:
    Graph *graph;
    Path *leastCostPath = new Path();
    std::list<std::pair<unsigned int, double>> *adjacencyList;
    std::vector<std::string> labelList;
    double leastCost;

    /**
     * Gera a lista de adjacências do grafo fornecido pelo construtor.
     * @return Erro encontrado ou error::ok em caso de sucesso.
     */
    errorType
    _GenerateAdjacencyList ();

    public:
    /**
     * Construtor da classe Dijkstra
     * @param graph Grafo o qual será executado o algoritmo.
     */
    Dijkstra (Graph *);
    
    ~Dijkstra ();

    /**
     * Executa o algoritmo de Dijkstra no grafo fornecido pelo construtor e guarda o resultado em atributos internos.
     * @param from vértice de partida
     * @param to vértice de chegada
     */
    errorType
    Calculate (std::string, std::string);

    /**
     * Obter o caminho com o menor custo calculado em Dijkstra::Calculate()
     * @return o Path * com o caminho de menor custo.
     */
    Path *
    GetLeastCostPath ();

    /**
     * Obtém o menor custo entre os vértices fornecidos em Dijkstra::Calculate()
     * @return O menor custo.
     */
    double
    GetLeastCost ();

    /**
     * Obtém o diâmetro do grafo alimentado ao Dijkstra.
     * @return O caminho de menor custo com o maior custo
     */
    Path *
    GetDiameter ();

    /**
     * Obtém o vértice de maior centralidade de intermediação do grafo alimentado ao Dijkstra
     * @return O vértice que faz parte do maior número de caminhos de menores custos.
     */
    Vertex *
    HighestBetweennessCentrality ();
};

#endif